package com.saas.api.op.test2.servant;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test2.protocol.input.*;
import com.saas.api.op.test2.protocol.output.*;

import com.github.pagehelper.PageInfo;
import com.saas.api.op.test2.servant.fallback.Test2ServantFallback;
import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.protocol.output.Test2Output;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@FeignClient(name = "saas-kernel-test2-servant", fallbackFactory = Test2ServantFallback.class)
public interface Test2Servant {
	
	


	/**
	 * 新增
	 */
	@RequestMapping(value = "/open/api/test2/addTest2", method = RequestMethod.POST)
	BaseOutput<Test2Output> addTest2(Test2Input test2Input);





}
