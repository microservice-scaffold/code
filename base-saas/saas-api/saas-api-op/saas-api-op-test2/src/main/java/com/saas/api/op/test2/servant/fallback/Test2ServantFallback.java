package com.saas.api.op.test2.servant.fallback;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test2.protocol.input.*;
import com.saas.api.op.test2.protocol.output.*;
import com.saas.api.op.test2.servant.Test2Servant;
import com.github.pagehelper.PageInfo;
import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.protocol.output.Test2Output;
import com.saas.api.op.test2.servant.Test2Servant;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class Test2ServantFallback implements FallbackFactory<Test2Servant> {

	Logger logger = LoggerFactory.getLogger(Test2ServantFallback.class);


	@Override
	public Test2Servant create(Throwable cause) {
		return new Test2Servant() {


			@Override
			public BaseOutput<Test2Output> addTest2(Test2Input test2Input) {
				logger.error("调用新增接口异常了，test2Input：{}", test2Input);
				return new BaseOutput<>(RetCode.FAIL.retCode, RetCode.FAIL.message);
			}


		};
	}
}
