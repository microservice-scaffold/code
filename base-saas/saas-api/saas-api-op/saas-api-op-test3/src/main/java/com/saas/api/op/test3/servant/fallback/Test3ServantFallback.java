package com.saas.api.op.test3.servant.fallback;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test3.protocol.input.*;
import com.saas.api.op.test3.protocol.output.*;
import com.saas.api.op.test3.servant.Test3Servant;
import com.github.pagehelper.PageInfo;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class Test3ServantFallback implements FallbackFactory<Test3Servant> {

	Logger logger = LoggerFactory.getLogger(Test3ServantFallback.class);


	@Override
	public Test3Servant create(Throwable cause) {
		return new Test3Servant() {


			@Override
			public BaseOutput<Test3Output> addTest3(Test3Input test3Input) {
				logger.error("调用新增接口异常了，test3Input：{}", test3Input);
				return new BaseOutput<>(RetCode.FAIL.retCode, RetCode.FAIL.message);
			}


		};
	}
}
