package com.saas.api.op.test3.servant;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test3.protocol.input.*;
import com.saas.api.op.test3.protocol.output.*;

import com.github.pagehelper.PageInfo;
import com.saas.api.op.test3.servant.fallback.Test3ServantFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@FeignClient(name = "saas-kernel-test3-servant", fallbackFactory = Test3ServantFallback.class)
public interface Test3Servant {
	
	


	/**
	 * 新增
	 */
	@RequestMapping(value = "/open/api/test3/addTest3", method = RequestMethod.POST)
	BaseOutput<Test3Output> addTest3(Test3Input test3Input);





}
