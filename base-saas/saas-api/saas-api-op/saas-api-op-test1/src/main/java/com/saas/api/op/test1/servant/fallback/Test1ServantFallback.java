package com.saas.api.op.test1.servant.fallback;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test1.protocol.input.*;
import com.saas.api.op.test1.protocol.output.*;
import com.saas.api.op.test1.servant.Test1Servant;
import com.github.pagehelper.PageInfo;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class Test1ServantFallback implements FallbackFactory<Test1Servant> {

	Logger logger = LoggerFactory.getLogger(Test1ServantFallback.class);


	@Override
	public Test1Servant create(Throwable cause) {
		return new Test1Servant() {


			@Override
			public BaseOutput<Test1Output> addTest1(Test1Input test1Input) {
				logger.error("调用新增接口异常了，test1Input：{}", test1Input);
				return new BaseOutput<>(RetCode.FAIL.retCode, RetCode.FAIL.message);
			}


		};
	}
}
