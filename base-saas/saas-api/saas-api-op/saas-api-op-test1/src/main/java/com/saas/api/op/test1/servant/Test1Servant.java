package com.saas.api.op.test1.servant;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test1.protocol.input.*;
import com.saas.api.op.test1.protocol.output.*;
import com.github.pagehelper.PageInfo;
import com.saas.api.op.test1.servant.fallback.Test1ServantFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@FeignClient(name = "saas-kernel-test1-servant", fallbackFactory = Test1ServantFallback.class)
public interface Test1Servant {
	
	


	/**
	 * 新增
	 */
	@RequestMapping(value = "/open/api/test1/addTest1", method = RequestMethod.POST)
	BaseOutput<Test1Output> addTest1(Test1Input test1Input);





}
