package com.saas.api.op.servicedemo.servant.fallback;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.api.op.servicedemo.servant.ServicedemoServant;


import com.github.pagehelper.PageInfo;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ServicedemoServantFallback implements FallbackFactory<ServicedemoServant> {

	Logger logger = LoggerFactory.getLogger(ServicedemoServantFallback.class);


	@Override
	public ServicedemoServant create(Throwable cause) {
		return new ServicedemoServant() {


			@Override
			public BaseOutput<ServicedemoOutput> addServicedemo(ServicedemoInput servicedemoInput) {
				logger.error("调用新增接口异常了，servicedemoInput：{}", servicedemoInput);
				return new BaseOutput<>(RetCode.FAIL.retCode, RetCode.FAIL.message);
			}


		};
	}
}
