package com.saas.api.op.servicedemo.protocol.input;

import lombok.Data;


@Data
public class ServicedemoInput {
	private Integer id;
	private String name;


}
