package com.saas.api.op.servicedemo.servant;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.api.op.servicedemo.servant.fallback.ServicedemoServantFallback;


import com.github.pagehelper.PageInfo;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 */
@FeignClient(name = "saas-kernel-servicedemo-servant", fallbackFactory = ServicedemoServantFallback.class)
public interface ServicedemoServant {
	
	


	/**
	 * 新增
	 */
	@RequestMapping(value = "/open/api/servicedemo/addServicedemo", method = RequestMethod.POST)
	BaseOutput<ServicedemoOutput> addServicedemo(ServicedemoInput servicedemoInput);





}
