package com.saas.ability.configure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class ConfigureServerApplication {
	
	public static void main(String[] args) {
//		SpringApplication.run(ConfigureServerApplication.class, args);
		SpringApplication app = new SpringApplication(ConfigureServerApplication.class);
		app.setWebEnvironment(true);
		app.run(args);
	}
}
