package com.saas.ability.gateway.base.protocol.input;

import lombok.Data;

/**
 * 版本校验
 *
 */
@Data
public class ReqHeaderInput {
	
	/**
	 * 请求来源
	 */
	private Integer fromType;
	
	/**
	 * 请求时间戳
	 */
	private Long timestamp;
	
	/**
	 * 设备类型
	 */
	private Integer deviceType;

	/**
	 * 终端类型 1：android 2：ios 3：其他
	 */
	private Integer terminalType;

	/**
	 * 设备编码
	 */
	private String deviceSn;
	
	/**
	 * 终端app版本号
	 */
	private String appVersion;
	
	/**
	 * 终端设备系统版本
	 */
	private String deviceVersion;
	
	/**
	 * 请求API的版本号
	 */
	private String apiVersion;
	
	/**
	 * 访问令牌
	 */
	private String accessToken;
	
	/**
	 * 签名
	 */
	private String signature;

}
