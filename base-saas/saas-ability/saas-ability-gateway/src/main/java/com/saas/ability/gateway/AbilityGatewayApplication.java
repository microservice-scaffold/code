package com.saas.ability.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;


@SpringCloudApplication
@EnableZuulProxy
@EnableHystrix
@EnableFeignClients(basePackages={"com.saas"})
@ComponentScan(basePackages={"com.saas"})
//@EnableMongoRepositories(basePackages={"com.saas"})
//todo 开启mongodb
public class AbilityGatewayApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AbilityGatewayApplication.class, args);
	}
}
