package com.saas.ability.gateway.filter;

import com.saas.ability.gateway.base.configure.VersionWriteListProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.util.RetCodeUtil;
import com.saas.ability.gateway.base.protocol.input.ReqHeaderInput;
import com.netflix.zuul.context.RequestContext;

/**
 * 版本检测
 *
 */
@Component
public class VersionFilter extends AbstractFilter {
	
	Logger logger = LoggerFactory.getLogger(VersionFilter.class);

//	@Autowired
//	VersionVerifyService versionVerifyService;
	//todo 版本相关service
	@Autowired
	VersionWriteListProperties versionWriteListProperties;

	private final PathMatcher pathMatcher = new AntPathMatcher();

	/**
	 * 1：如果入口就确定不做版本校验，则意味不做版本校验
	 * 2：如果入口确定做版本校验，在该过滤器是白名单地址则亦不做版本校验
	 */
	@Override
	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		String uri = ctx.getRequest().getRequestURI();
		if(!ctx.getBoolean(VERIFY_VERSION)){
			return false; 
		}else if(isWhitePath(uri)){
			return false;
		}
		return true;
	}
	
	/**
	 * 是否白名单地址
	 * @param requestUri
	 * @return
	 */
	public boolean isWhitePath(String requestUri){
		String writeList = versionWriteListProperties.getWriteList();
		if(!StringUtils.isEmpty(writeList)){
			String[] writeArr = writeList.split(",");
			for(String url : writeArr){
				if(pathMatcher.match(url, requestUri)){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		ReqHeaderInput reqHeaderInput = (ReqHeaderInput)ctx.get(HEADER);
		
//		BaseOutput<VersionInfoOutput> output = verifyParams(reqHeaderInput);
//		if(!RetCodeUtil.isSucc(output.getRetCode())){
//			logger.warn("没有获得需要的header信息，不合法的接口请求");
//			handleResponse(ctx,output);
//			return null;
//		}
//
//		output = versionVerifyService.verify(reqHeaderInput.getDeviceType(),reqHeaderInput.getTerminalType(),
//				reqHeaderInput.getDeviceSn(),reqHeaderInput.getAppVersion());
//		if(!RetCodeUtil.isSucc(output.getRetCode())){
//			handleResponse(ctx, output);
//		}
		//todo 版本相关判断
		return null;
	}
	
//	public BaseOutput<VersionInfoOutput> verifyParams(ReqHeaderInput reqHeaderInput){
//		if(reqHeaderInput == null){
//			logger.error("版本校验，没有获得需要的header信息，不合法的接口请求");
//			return new BaseOutput<>(RetCode.PARAM_INVALID.retCode,"版本校验，没有获得需要的header信息，不合法的接口请求");
//		}
//		if(StringUtils.isEmpty(reqHeaderInput.getDeviceType())){
//			logger.error("版本校验，deviceType参数不能为空");
//			return new BaseOutput<>(RetCode.PARAM_INVALID.retCode,"deviceType不能为空");
//		}
//		if(StringUtils.isEmpty(reqHeaderInput.getTerminalType())){
//			logger.error("版本校验，terminalType参数不能为空");
//			return new BaseOutput<>(RetCode.PARAM_INVALID.retCode,"terminalType参数不能为空");
//		}
//		if(StringUtils.isEmpty(reqHeaderInput.getDeviceSn())){
//			logger.error("版本校验，deviceSn参数不能为空");
//			return new BaseOutput<>(RetCode.PARAM_INVALID.retCode,"deviceSn不能为空");
//		}
//		if(StringUtils.isEmpty(reqHeaderInput.getAppVersion())){
//			logger.error("版本校验，appVersion参数不能为空");
//			return new BaseOutput<>(RetCode.PARAM_INVALID.retCode,"appVersion参数不能为空");
//		}
//		return new BaseOutput<>(RetCode.SUCCESS.retCode,RetCode.SUCCESS.message);
//	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return Integer.MIN_VALUE + 2;
	}




}
