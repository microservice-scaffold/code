package com.saas.ability.gateway.base.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="gateaway.version")
@Data
public class VersionWriteListProperties {

	/**
	 * 版本校验白名单 多个逗号分隔
	 */
	private String writeList;
	
}
