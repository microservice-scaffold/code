package com.saas.ability.gateway.base.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@Component
public class ApplicationDestroy implements DisposableBean {
	
	Logger logger  = LoggerFactory.getLogger(ApplicationDestroy.class);

	@Override
	public void destroy() throws Exception {
		System.out.println("==========ApplicationDestroy============");
	}
}
