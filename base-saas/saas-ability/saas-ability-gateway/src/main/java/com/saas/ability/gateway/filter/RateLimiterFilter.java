package com.saas.ability.gateway.filter;

import org.springframework.stereotype.Component;

/**
 * API访问频率
 *
 */
@Component
public class RateLimiterFilter extends AbstractFilter {
	
	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public boolean shouldFilter() {
		return false;
	}

	@Override
	public Object run() {
		return null;
	}

	@Override
	public int filterOrder() {
		return Integer.MIN_VALUE + 1;
	}

	
}
