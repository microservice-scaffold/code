package com.saas.ability.gateway.filter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
 * 统一处理错误
 *
 */
@Component
public class UnifyErrorFilter extends ZuulFilter {

	Logger logger = LoggerFactory.getLogger(UnifyErrorFilter.class);

	private static final String ERROR_STATUS_CODE_KEY = "error.status_code";
	public static final String DEFAULT_ERR_MSG = "系统繁忙,请稍后再试";

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		return null;
	}


	@Override
	public String filterType() {
		return "error";
	}

	@Override
	public int filterOrder() {
		return Integer.MAX_VALUE;
	}

}
