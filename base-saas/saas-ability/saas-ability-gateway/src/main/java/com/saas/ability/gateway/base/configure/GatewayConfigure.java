package com.saas.ability.gateway.base.configure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.saas.ability.core.base.event.EventBus;
import com.saas.ability.core.base.thread.ThreadPoolConfig;
import com.saas.ability.core.base.thread.ThreadPoolManager;

@Configuration
@EnableConfigurationProperties({VersionWriteListProperties.class })
public class GatewayConfigure {

	@Bean
	public ThreadPoolConfig threadPoolConfig(){
		int poolSize = Runtime.getRuntime().availableProcessors();
		ThreadPoolConfig config = new ThreadPoolConfig();
		config.setCorePoolSize(poolSize);
		config.setMaxPoolSize(2 * poolSize);
		config.setKeepAliveSeconds(100);
		config.setQueueCapacity(10000);
		config.setRejectedPolicy(ThreadPoolConfig.REJECTED_POLICY_ABORT);
		return config;
	}
	
	@Bean
	public ThreadPoolManager threadPoolManager(ThreadPoolConfig config){
		return new ThreadPoolManager(config);
	}

	@Autowired
	public void initEventBus(ThreadPoolManager threadPoolManager) {
        EventBus.create(threadPoolManager.getEventBusExecutor());
    }

}
