package com.saas.ability.registry.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;

public class ApplicationDestroy implements DisposableBean {
	
	Logger logger  = LoggerFactory.getLogger(ApplicationDestroy.class);
	
	@Override
	public void destroy() throws Exception {
		logger.info("System [saas-ability-configure] destroy");
	}
}
