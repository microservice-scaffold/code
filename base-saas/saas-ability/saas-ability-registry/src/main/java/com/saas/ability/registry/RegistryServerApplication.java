package com.saas.ability.registry;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class RegistryServerApplication {
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(RegistryServerApplication.class).web(true).run(args);
	}
}
