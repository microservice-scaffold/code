package com.saas.ability.registry.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;

public class StartupRunner implements CommandLineRunner {
	
	Logger logger  = LoggerFactory.getLogger(StartupRunner.class);

	@Value("${spring.profiles.active:default}")
	private String env;
	
	@Value("${spring.cloud.config.profile:default}")
	private String configEnv;
	
	@Override
    public void run(String... args) throws Exception {
		logger.warn("注册中心服务器启动....");
		logger.warn("spring.profiles.active:{}",env);
		logger.warn("spring.cloud.config.profile:{}",configEnv);
		logger.warn("注册中心服务器启动成功");
    }
}
