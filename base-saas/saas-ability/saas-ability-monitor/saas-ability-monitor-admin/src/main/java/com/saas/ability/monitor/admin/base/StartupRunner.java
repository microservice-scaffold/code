package com.saas.ability.monitor.admin.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {
	
	Logger logger  = LoggerFactory.getLogger(StartupRunner.class);
	
	@Value("${spring.profiles.active}")
	private String env;
	
	@Value("${spring.cloud.config.profile}")
	private String configEnv;
	
	@Override
    public void run(String... args) throws Exception {
		logger.debug("监控服务器启动....");
		logger.debug("spring.profiles.active:{}",env);
		logger.debug("spring.cloud.config.profile:{}",configEnv);
		logger.debug("监控服务器启动成功");
    }
}
