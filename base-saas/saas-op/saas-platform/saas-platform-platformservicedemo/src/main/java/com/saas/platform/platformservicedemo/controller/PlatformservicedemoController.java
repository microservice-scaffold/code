package com.saas.platform.platformservicedemo.controller;

import java.util.ArrayList;
import java.util.List;


import com.saas.ability.core.base.protocol.BaseOutput;

import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.platform.platformservicedemo.service.PlatformservicedemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/open/api/platformservicedemo")
public class PlatformservicedemoController {
	
	Logger logger = LoggerFactory.getLogger(PlatformservicedemoController.class);
	
	@Autowired
	private PlatformservicedemoService platformservicedemoService;
	

	@RequestMapping("/addPlatformservicedemo")
	public BaseOutput<ServicedemoOutput> addPlatformservicedemo(@RequestBody ServicedemoInput servicedemoInput){


		return platformservicedemoService.addPlatformservicedemo(servicedemoInput);
	}




}
