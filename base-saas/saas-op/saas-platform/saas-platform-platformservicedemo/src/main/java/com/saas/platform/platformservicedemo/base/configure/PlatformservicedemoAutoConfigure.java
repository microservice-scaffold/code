package com.saas.platform.platformservicedemo.base.configure;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.saas.ability.core.base.http.RpcClient;

@Configuration
public class PlatformservicedemoAutoConfigure {

	Logger logger = LoggerFactory.getLogger(PlatformservicedemoAutoConfigure.class);


	@Bean
	public RpcClient createRpcClient() {
		return new RpcClient();
	}
	
	@Bean
	public ExecutorService createExecutorService(){
		return Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
	}

}
