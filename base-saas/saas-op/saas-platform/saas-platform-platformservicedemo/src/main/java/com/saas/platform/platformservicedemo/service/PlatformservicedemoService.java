package com.saas.platform.platformservicedemo.service;


import java.util.List;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;


public interface PlatformservicedemoService {
	/**
	 * 新增
	 */
	BaseOutput<ServicedemoOutput> addPlatformservicedemo(ServicedemoInput servicedemoInput);




}
