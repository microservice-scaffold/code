package com.saas.platform.platformservicedemo.base.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;



/**
 * accessToken鉴权拦截
 *
 **/
@Configuration
public class AuthTokenInterceptor extends HandlerInterceptorAdapter {

    Logger logger = LoggerFactory.getLogger(AuthTokenInterceptor.class);

//    @Autowired
//    MemberCacheService userCacheService;

    /**
     * 在请求处理之前进行调用（Controller方法调用之前）
     *
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("[AuthTokenInterceptor]-[preHandle]拦截用户登录会话");

        String accessToken = request.getParameter("accessToken");

        logger.debug(">>>>>>>>>>>>>> accessToken:{}", accessToken);
//        if (StringUtils.isEmpty(accessToken)) {
//            logger.warn("未登陆的用户访问了需要鉴权的接口");
//            throw new LoginTokenException("鉴权失败，请登陆");
//        }
//        //从缓存获取登陆用户信息
//        MemberGroupCache loginUserInfo = userCacheService.getLoginMemberGroupInfo(accessToken);
//        logger.debug(">>>>>>>>>>>>>> loginUserInfo:{}", loginUserInfo);
//        if (loginUserInfo == null) {
//            logger.debug("accessToken:{}", accessToken);
//            logger.debug("登录信息不存在或已失效,请重新登录");
//            throw new LoginTokenException("登录信息不存在或已失效,请重新登录");
//        }
//        //缓存本地线程
//        LocalThreadUser.setMemberCache(loginUserInfo);
        return true;
    }

    /**
     * 完成
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        try {
//            LocalThreadUser.remove();
        } catch (Exception e) {
            logger.error("接口请求日志拦截器错误[end]:", e);
        }
    }

}
