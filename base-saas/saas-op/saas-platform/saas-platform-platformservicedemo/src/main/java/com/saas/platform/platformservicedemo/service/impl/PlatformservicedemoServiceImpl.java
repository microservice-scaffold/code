package com.saas.platform.platformservicedemo.service.impl;

import java.util.ArrayList;
import java.util.List;


import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;

import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.api.op.servicedemo.servant.ServicedemoServant;
import com.saas.platform.platformservicedemo.service.PlatformservicedemoService;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;



@Component
public class PlatformservicedemoServiceImpl implements PlatformservicedemoService {

	Logger logger = LoggerFactory.getLogger(PlatformservicedemoServiceImpl.class);

	@Autowired
	private ServicedemoServant servicedemoServant;



	@Override
	@GlobalTransactional(rollbackFor = Exception.class)
	public BaseOutput<ServicedemoOutput> addPlatformservicedemo(ServicedemoInput servicedemoInput) {
		ServicedemoInput servicedemoInput2=new ServicedemoInput();
		servicedemoInput2.setId((int)System.currentTimeMillis());
		servicedemoInput2.setName("测试");
		BaseOutput<ServicedemoOutput> o=servicedemoServant.addServicedemo(servicedemoInput2);//测试调用远程服务，此处直接写死数据



		if(!o.getRetCode().equals("100")){
			String s=null;
			s.endsWith("");
		}
		//上述代码为远程服务异常时，根据返回的状态码，做对应处理
		// 此处为异常后全局事务回滚，可自从用更优雅的方式回滚
		//也可以根据需要，由返回的状态码决定是否降级处理，不采取任何操作
		//异常状态码来源于对应服务的fallbackFactory

		//		String s=null;
		//		s.endsWith("");
		//取消上述注释，测试本地服务的全局事务回滚


		ServicedemoOutput output=new ServicedemoOutput();
		output.setId((int)System.currentTimeMillis());
		output.setName("测试");//此处直接写死返回数据

		return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


	}


}
