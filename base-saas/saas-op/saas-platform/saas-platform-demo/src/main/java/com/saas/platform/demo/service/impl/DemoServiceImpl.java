package com.saas.platform.demo.service.impl;

import java.util.ArrayList;
import java.util.List;


import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;
import com.saas.api.op.test1.servant.Test1Servant;
import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test3.protocol.input.Test3Input;
import com.saas.api.op.test3.servant.Test3Servant;
import com.saas.op.base.dao.mysql.domain.Test1;
import com.saas.platform.demo.service.DemoService;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;



@Component
public class DemoServiceImpl implements DemoService {

	Logger logger = LoggerFactory.getLogger(DemoServiceImpl.class);

	@Autowired
	private Test1Servant test1Servant;
	@Autowired
	private Test3Servant test3Servant;


	@Override
	@GlobalTransactional(rollbackFor = Exception.class)
	public BaseOutput<Test1Output> addTest1(Test1Input test1Input) {
		Test3Input test3Input=new Test3Input();
		test3Input.setId((int)System.currentTimeMillis());
		test3Input.setName("测试");
		test3Servant.addTest3(test3Input);//测试调用远程服务，此处直接写死数据

		Test1Input test11Input=new Test1Input();
		test11Input.setId((int)System.currentTimeMillis());
		test11Input.setName("测试");
		BaseOutput<Test1Output> o=test1Servant.addTest1(test1Input);//测试调用远程服务，此处直接写死数据

		if(!o.getRetCode().equals("100")){
			String s=null;
			s.endsWith("");
		}
		//上述代码为远程服务异常时，根据返回的状态码，做对应处理
		// 此处为异常后全局事务回滚，可自从用更优雅的方式回滚
		//也可以根据需要，由返回的状态码决定是否降级处理，不采取任何操作
		//异常状态码来源于对应服务的fallbackFactory

		//		String s=null;
		//		s.endsWith("");
		//取消上述注释，测试本地服务的全局事务回滚


		Test1Output output=new Test1Output();
		output.setId((int)System.currentTimeMillis());
		output.setName("测试");//此处直接写死返回数据

		return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


	}


}
