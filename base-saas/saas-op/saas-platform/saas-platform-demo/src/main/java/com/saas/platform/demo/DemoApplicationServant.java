package com.saas.platform.demo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SpringCloudApplication
@EnableHystrix
@EnableFeignClients(basePackages = {"com.saas"})
@ComponentScan(basePackages={"com.saas"})
//@EnableMongoRepositories(basePackages={"com.saas","com.saas"})
public class DemoApplicationServant {
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(DemoApplicationServant.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.setWebEnvironment(true);
		app.run(args);
	}
}
