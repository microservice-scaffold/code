package com.saas.platform.demo.base.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {
	
	Logger logger  = LoggerFactory.getLogger(StartupRunner.class);
	
	@Value("${spring.profiles.active}")
	private String env;
	
	@Value("${spring.cloud.config.profile}")
	private String configEnv;
	
	
	
	@Override
    public void run(String... args) throws Exception {
		logger.info("demo服务器启动....");
		logger.info("spring.profiles.active:{}",env);
		logger.info("spring.cloud.config.profile:{}",configEnv);
		logger.info("demo服务器启动成功");
    }
}
