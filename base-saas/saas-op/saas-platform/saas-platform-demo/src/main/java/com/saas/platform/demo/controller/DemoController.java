package com.saas.platform.demo.controller;

import java.util.ArrayList;
import java.util.List;


import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;
import com.saas.api.op.test3.protocol.output.Test3Output;
import com.saas.platform.demo.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/open/api/demo")
public class DemoController {
	
	Logger logger = LoggerFactory.getLogger(DemoController.class);
	
	@Autowired
	private DemoService demoService;
	

	@RequestMapping("/addTest1")
	public BaseOutput<Test1Output> addTest1(@RequestBody Test1Input test1Input){


		return demoService.addTest1(test1Input);
	}




}
