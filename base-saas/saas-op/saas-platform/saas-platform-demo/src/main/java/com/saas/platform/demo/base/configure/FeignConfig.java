package com.saas.platform.demo.base.configure;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;



import feign.Request;
/** 

 */

@Configuration
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.saas"})
@ComponentScan(basePackages={"com.saas"})
public class FeignConfig {

	@Bean
    public static Request.Options requestOptions(ConfigurableEnvironment env) {
		int ribbonReadTimeout = env.getProperty("ribbon.ReadTimeout", int.class, 13000);
        int ribbonConnectionTimeout = env.getProperty("ribbon.ConnectTimeout", int.class, 15000);
        return new Request.Options(ribbonConnectionTimeout, ribbonReadTimeout);
    }
}
