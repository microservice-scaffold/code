package com.saas.platform.demo.service;


import java.util.List;

import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;


public interface DemoService {
	/**
	 * 新增
	 */
	BaseOutput<Test1Output> addTest1(Test1Input test1Input);




}
