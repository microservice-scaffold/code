package com.saas.kernel.test3.servant.controller;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;

import com.saas.api.op.test3.protocol.input.Test3Input;
import com.saas.api.op.test3.protocol.output.Test3Output;
import com.saas.kernel.test3.servant.service.Test3Service;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

/**

 */
@RestController
@RequestMapping("/open/api/test3")
public class Test3Controller {
    Logger logger = LoggerFactory.getLogger(Test3Controller.class);

    @Autowired
    Test3Service test3Service;

    /**
     * 新增
     */
    @RequestMapping(value = "/addTest3")
    public BaseOutput<Test3Output> addTest3(@RequestBody Test3Input test3Input) {

        return test3Service.addTest3(test3Input);
    }




}
