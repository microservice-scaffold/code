package com.saas.kernel.test3.servant.service;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test3.protocol.input.Test3Input;
import com.saas.api.op.test3.protocol.output.Test3Output;

import java.util.List;

/**

 */
public interface Test3Service {




    /**
     * 新增
     */
    BaseOutput<Test3Output> addTest3(Test3Input test3Input);





}

