package com.saas.kernel.test3.servant.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;


import com.saas.api.op.test3.protocol.input.Test3Input;
import com.saas.api.op.test3.protocol.output.Test3Output;
import com.saas.kernel.test3.servant.service.Test3Service;
import com.saas.op.base.cache.service.Test3CacheService;
import com.saas.op.base.dao.mysql.domain.Test3;
import com.saas.op.base.dao.mysql.mapper.test3.Test3Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.redisson.api.RLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**

 */
@Component
public class Test3ServiceImpl implements Test3Service {

    Logger logger = LoggerFactory.getLogger(Test3Service.class);



    @Autowired
    Test3CacheService test3CacheService;



    @Autowired
    ExecutorService executorService;



    @Autowired
    Test3Mapper test3Mapper;



    @Override
    public BaseOutput<Test3Output> addTest3(Test3Input test3Input) {

        Test3 test3=new Test3();
        test3.setId((int)System.currentTimeMillis());
        test3.setName("测试");
        test3Mapper.insert(test3);
        Test3Output output=new Test3Output();
        output.setId((int)System.currentTimeMillis());
        output.setName("测试");
        return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


    }





}
