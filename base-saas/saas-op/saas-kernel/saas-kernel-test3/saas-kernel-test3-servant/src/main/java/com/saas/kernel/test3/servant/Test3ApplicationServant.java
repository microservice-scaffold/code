package com.saas.kernel.test3.servant;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
//@EnableTransactionManagement
@EnableDiscoveryClient
@EnableHystrix
@EnableCircuitBreaker
@EnableFeignClients(basePackages = { "com.saas" })
@ComponentScan(basePackages = { "com.saas" })
//@EnableMongoRepositories(basePackages={"com.saas.op.base.dao.mongodb","com.saas.data.dao"})
public class Test3ApplicationServant {
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Test3ApplicationServant.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.setWebEnvironment(true);
		app.run(args);
	}
}
