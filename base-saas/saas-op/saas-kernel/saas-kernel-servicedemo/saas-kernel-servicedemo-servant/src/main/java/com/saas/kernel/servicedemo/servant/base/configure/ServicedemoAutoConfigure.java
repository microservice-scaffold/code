package com.saas.kernel.servicedemo.servant.base.configure;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicedemoAutoConfigure {

	Logger logger = LoggerFactory.getLogger(ServicedemoAutoConfigure.class);
	
	
	@Bean
    public ExecutorService getThreadPool(){
        return Executors.newFixedThreadPool(5);
    }
	
	
}
