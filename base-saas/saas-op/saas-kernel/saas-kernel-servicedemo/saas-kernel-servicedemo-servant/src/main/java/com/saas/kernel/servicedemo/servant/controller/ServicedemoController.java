package com.saas.kernel.servicedemo.servant.controller;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;


import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.kernel.servicedemo.servant.service.ServicedemoService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

/**

 */
@RestController
@RequestMapping("/open/api/servicedemo")
public class ServicedemoController {
    Logger logger = LoggerFactory.getLogger(ServicedemoController.class);

    @Autowired
    ServicedemoService servicedemoService;

    /**
     * 新增
     */
    @RequestMapping(value = "/addServicedemo")
    public BaseOutput<ServicedemoOutput> addServicedemo(@RequestBody ServicedemoInput servicedemoInput) {

        return servicedemoService.addServicedemo(servicedemoInput);
    }




}
