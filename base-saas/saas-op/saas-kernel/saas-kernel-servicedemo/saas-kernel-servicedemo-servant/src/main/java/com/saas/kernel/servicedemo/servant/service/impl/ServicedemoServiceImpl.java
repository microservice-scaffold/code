package com.saas.kernel.servicedemo.servant.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;


import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;
import com.saas.kernel.servicedemo.servant.service.ServicedemoService;
import com.saas.op.base.cache.service.ServicedemoCacheService;
import com.saas.op.base.dao.mysql.domain.Servicedemo;
import com.saas.op.base.dao.mysql.mapper.servicedemo.ServicedemoMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.redisson.api.RLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**

 */
@Component
public class ServicedemoServiceImpl implements ServicedemoService {

    Logger logger = LoggerFactory.getLogger(ServicedemoService.class);



    @Autowired
    ServicedemoCacheService servicedemoCacheService;



    @Autowired
    ExecutorService executorService;



    @Autowired
    ServicedemoMapper servicedemoMapper;



    @Override
    public BaseOutput<ServicedemoOutput> addServicedemo(ServicedemoInput servicedemoInput) {

        Servicedemo servicedemo=new Servicedemo();
        servicedemo.setId((int)System.currentTimeMillis());
        servicedemo.setName("测试");
        servicedemoMapper.insert(servicedemo);
        ServicedemoOutput output=new ServicedemoOutput();
        output.setId((int)System.currentTimeMillis());
        output.setName("测试");
        return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


    }





}
