package com.saas.kernel.servicedemo.servant.service;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.servicedemo.protocol.input.ServicedemoInput;
import com.saas.api.op.servicedemo.protocol.output.ServicedemoOutput;


import java.util.List;

/**

 */
public interface ServicedemoService {




    /**
     * 新增
     */
    BaseOutput<ServicedemoOutput> addServicedemo(ServicedemoInput servicedemoInput);





}

