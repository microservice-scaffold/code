package com.saas.kernel.test1.servant.controller;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;
import com.saas.kernel.test1.servant.service.Test1Service;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

/**

 */
@RestController
@RequestMapping("/open/api/test1")
public class Test1Controller {
    Logger logger = LoggerFactory.getLogger(Test1Controller.class);

    @Autowired
    Test1Service test1Service;

    /**
     * 新增
     */
    @RequestMapping(value = "/addTest1")
    public BaseOutput<Test1Output> addTest1(@RequestBody Test1Input test1Input) {

        return test1Service.addTest1(test1Input);
    }




}
