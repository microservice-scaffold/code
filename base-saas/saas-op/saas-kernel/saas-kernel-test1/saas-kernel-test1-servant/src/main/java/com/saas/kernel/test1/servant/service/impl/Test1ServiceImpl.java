package com.saas.kernel.test1.servant.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;
import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.servant.Test2Servant;
import com.saas.kernel.test1.servant.service.Test1Service;
import com.saas.op.base.cache.service.Test1CacheService;
import com.saas.op.base.dao.mysql.domain.Test1;
import com.saas.op.base.dao.mysql.domain.Test2;
import com.saas.op.base.dao.mysql.mapper.test1.Test1Mapper;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.redisson.api.RLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;

import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**

 */
@Component
public class Test1ServiceImpl implements Test1Service {

    Logger logger = LoggerFactory.getLogger(Test1Service.class);



    @Autowired
    Test1CacheService test1CacheService;



    @Autowired
    ExecutorService executorService;

    @Autowired
    Test1Mapper test1Mapper;

    @Autowired
    Test2Servant test2Servant;



    @Override
    public BaseOutput<Test1Output> addTest1(Test1Input test1Input) {

        test1CacheService.cacheTest1(null);//测试redis缓存的写读，具体查看service实现类

        Test1 test1=new Test1();
        test1.setId((int)System.currentTimeMillis());
        test1.setName("测试");
        test1Mapper.insert(test1);//测试本服务本地写，省略Test1Input--》Test1的转化，直接写死数据

        Test2Input test2Input=new Test2Input();
        test2Input.setId((int)System.currentTimeMillis());
        test2Input.setName("测试");
        test2Servant.addTest2(test2Input);//测试依赖服务远程写，直接写死数据

//        String s=null;
//        s.endsWith("");
        //取消上述注释，测试依赖服务的全局事务回滚

        Test1Output output=new Test1Output();
        output.setId((int)System.currentTimeMillis());
        output.setName("测试");//测试返回，直接写死数据

        return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


    }





}
