package com.saas.kernel.test1.servant.service;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test1.protocol.input.Test1Input;
import com.saas.api.op.test1.protocol.output.Test1Output;

import java.util.List;

/**

 */
public interface Test1Service {




    /**
     * 新增
     */
    BaseOutput<Test1Output> addTest1(Test1Input test1Input);





}

