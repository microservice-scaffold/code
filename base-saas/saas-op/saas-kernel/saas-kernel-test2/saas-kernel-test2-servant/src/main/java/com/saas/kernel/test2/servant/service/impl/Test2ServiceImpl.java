package com.saas.kernel.test2.servant.service.impl;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.ability.core.base.protocol.SimpleOutput;

import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.protocol.output.Test2Output;
import com.saas.api.op.test2.servant.Test2Servant;
import com.saas.kernel.test2.servant.service.Test2Service;
import com.saas.op.base.cache.service.Test2CacheService;
import com.saas.op.base.dao.mysql.domain.Test2;
import com.saas.op.base.dao.mysql.mapper.test2.Test2Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.redisson.api.RLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**

 */
@Component
public class Test2ServiceImpl implements Test2Service {

    Logger logger = LoggerFactory.getLogger(Test2Service.class);



    @Autowired
    Test2CacheService test2CacheService;



    @Autowired
    ExecutorService executorService;



    @Autowired
    Test2Mapper test2Mapper;



    @Override
    public BaseOutput<Test2Output> addTest2(Test2Input test2Input) {

        Test2 test2=new Test2();
        test2.setId((int)System.currentTimeMillis());
        test2.setName("测试");
        test2Mapper.insert(test2);
        Test2Output output=new Test2Output();
        output.setId((int)System.currentTimeMillis());
        output.setName("测试");
        return new BaseOutput<>(RetCode.SUCCESS.retCode, RetCode.SUCCESS.message, output);


    }





}
