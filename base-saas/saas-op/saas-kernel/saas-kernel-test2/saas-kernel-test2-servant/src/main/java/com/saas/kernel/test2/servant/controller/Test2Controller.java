package com.saas.kernel.test2.servant.controller;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;

import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.protocol.output.Test2Output;
import com.saas.kernel.test2.servant.service.Test2Service;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

/**

 */
@RestController
@RequestMapping("/open/api/test2")
public class Test2Controller {
    Logger logger = LoggerFactory.getLogger(Test2Controller.class);

    @Autowired
    Test2Service test2Service;

    /**
     * 新增
     */
    @RequestMapping(value = "/addTest2")
    public BaseOutput<Test2Output> addTest2(@RequestBody Test2Input test2Input) {

        return test2Service.addTest2(test2Input);
    }




}
