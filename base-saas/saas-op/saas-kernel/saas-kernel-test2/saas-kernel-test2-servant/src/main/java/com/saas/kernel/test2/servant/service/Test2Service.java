package com.saas.kernel.test2.servant.service;


import com.github.pagehelper.PageInfo;
import com.saas.ability.core.base.protocol.BaseOutput;
import com.saas.api.op.test2.protocol.input.Test2Input;
import com.saas.api.op.test2.protocol.output.Test2Output;

import java.util.List;

/**

 */
public interface Test2Service {




    /**
     * 新增
     */
    BaseOutput<Test2Output> addTest2(Test2Input test2Input);





}

