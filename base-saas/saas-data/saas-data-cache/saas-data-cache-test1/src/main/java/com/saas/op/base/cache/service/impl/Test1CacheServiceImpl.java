package com.saas.op.base.cache.service.impl;

import java.util.List;

import com.saas.data.cache.base.service.SimpleCacheService;
import com.saas.op.base.cache.entity.Test1Cache;
import com.saas.op.base.cache.service.Test1CacheService;
import com.saas.op.base.dao.mysql.domain.Test1;
import com.saas.op.base.dao.mysql.mapper.test1.Test1Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**

 */
@Component("test1CacheService")
public class Test1CacheServiceImpl implements Test1CacheService {
	

	
	@Autowired
	private Test1Mapper test1Mapper;

	@Autowired
	private SimpleCacheService simpleCacheService;



	//测试redis缓存的写与读
	@Override
	public Test1Cache cacheTest1(Test1Cache test1Cache) {
		Test1 test1=test1Mapper.selectByPrimaryKey(1);
		Test1Cache test1Cache1=new Test1Cache();
		test1Cache1.setId(test1.getId());
		test1Cache1.setName(test1.getName());
		simpleCacheService.hAdd("testcachefortest1",test1Cache1.getId().toString(),test1Cache1);
		System.out.println(simpleCacheService.hGet("testcachefortest1",test1Cache1.getId().toString(),Test1Cache.class));
		return test1Cache1;
	}

	@Override
	public Test1Cache refreshTest1(Test1Cache test1Cache) {
		return null;
	}

	@Override
	public Test1Cache getTest1(Test1Cache test1Cache) {
		return null;
	}

	@Override
	public Test1Cache deleteTest1(Test1Cache test1Cache) {
		return null;
	}
}
