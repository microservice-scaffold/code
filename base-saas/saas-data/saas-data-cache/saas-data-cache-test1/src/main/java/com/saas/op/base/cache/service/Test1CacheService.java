package com.saas.op.base.cache.service;

import com.saas.op.base.cache.entity.Test1Cache;

/**

 */

public interface Test1CacheService {


    /**
     * 缓存

     */
    Test1Cache cacheTest1(Test1Cache test1Cache);


    /**
     * 刷新
     */
    Test1Cache refreshTest1(Test1Cache test1Cache);

    /**
     * 获取
     */
    Test1Cache getTest1(Test1Cache test1Cache);


    /**
     * 删除

     */
    Test1Cache deleteTest1(Test1Cache test1Cache);


}
