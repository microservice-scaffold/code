package com.saas.data.cache.base.service;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * redis set
 *
 */
@Component("redisSetCacheService")
public class RedisSetCacheService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public <T> long sAdd(String key, T val) {
        if (!StringUtils.isEmpty(val)) {
            return redisTemplate.opsForSet().add(key, JSON.toJSONString(val));
        }
        return 0;
    }

    public <T> T sGet(String key, Class<T> clazz) {
        String json = redisTemplate.opsForSet().pop(key);
        if (!StringUtils.isEmpty(json)) {
            return JSON.parseObject(json, clazz);
        }
        return null;
    }

    public <T> long sDel(String key, T val) {
        return redisTemplate.opsForSet().remove(key, JSON.toJSONString(val));
    }
}