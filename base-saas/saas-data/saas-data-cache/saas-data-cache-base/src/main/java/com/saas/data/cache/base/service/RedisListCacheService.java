package com.saas.data.cache.base.service;

import com.alibaba.fastjson.JSON;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * redis list
 *
 */
@Component("redisListCacheService")
public class RedisListCacheService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public <T> void lPush(String key, T val) {
        if (!StringUtils.isEmpty(val)) {
            redisTemplate.opsForList().leftPush(key, JSON.toJSONString(val));
        }
    }

    public <T> void rPush(String key, T val) {
        if (!StringUtils.isEmpty(val)) {
            redisTemplate.opsForList().rightPush(key, JSON.toJSONString(val));
        }
    }

    public <T> T lGet(String key, Class<T> clazz) {
        String json = redisTemplate.opsForList().leftPop(key);
        if (!StringUtils.isEmpty(json)) {
            return JSON.parseObject(json, clazz);
        }

        return null;
    }

    public <T> T rGet(String key, Class<T> clazz) {
        String json = redisTemplate.opsForList().rightPop(key);
        if (!StringUtils.isEmpty(json)) {
            return JSON.parseObject(json, clazz);
        }

        return null;
    }

    public long size(String key) {
        return redisTemplate.opsForList().size(key);
    }

    public <T> long remove(String key, T val) {
        return redisTemplate.opsForList().remove(key, 0, String.valueOf(val));
    }

    public <T> List<T> getAll(String key, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        List<String> stringList = redisTemplate.opsForList().range(key, 0, -1);
        if (!stringList.isEmpty()) {
            stringList.forEach(json -> {
                list.add(JSON.parseObject(json, clazz));
            });
        }
        return list;
    }

    public void del(String key) {
        redisTemplate.delete(key);
    }
}