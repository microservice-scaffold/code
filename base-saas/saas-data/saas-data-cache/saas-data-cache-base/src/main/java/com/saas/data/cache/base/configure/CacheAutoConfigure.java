package com.saas.data.cache.base.configure;

import javax.annotation.PreDestroy;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties.Pool;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import com.saas.data.cache.base.util.RedisLockUtil;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class CacheAutoConfigure {
	
	
	@Bean
	public StringRedisTemplate stringRedisTemplate(RedisProperties properties){
		StringRedisTemplate template = new StringRedisTemplate();
		JedisConnectionFactory redisConnectionFactory = jedisConnectionFactory(properties);
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}
	
	@Bean
	public JedisConnectionFactory jedisConnectionFactory(RedisProperties properties) {
		
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
		
		JedisPoolConfig jedisPoolConfig = jedisPoolConfig(properties);
        //连接池  
		jedisConnectionFactory.setPoolConfig(jedisPoolConfig);  
        //IP地址  
		jedisConnectionFactory.setHostName(properties.getHost());  
        //端口号  
		jedisConnectionFactory.setPort(properties.getPort());  
        //db
		jedisConnectionFactory.setDatabase(properties.getDatabase());
        
        //如果Redis设置有密码  
        if(!StringUtils.isEmpty(properties.getPassword())){
        	jedisConnectionFactory.setPassword(properties.getPassword()); 
        }
        //客户端超时时间单位是毫秒  
        jedisConnectionFactory.setTimeout(properties.getTimeout());  
        
        return jedisConnectionFactory; 
	}
	
	public JedisPoolConfig jedisPoolConfig(RedisProperties properties) {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        Pool pool = properties.getPool();
        if(pool != null){
        	// 最大空闲数
        	jedisPoolConfig.setMaxIdle(pool.getMaxIdle());
        	// 最小空闲数
        	jedisPoolConfig.setMinIdle(pool.getMaxIdle());
        	// 连接池的最大数据库连接数
        	jedisPoolConfig.setMaxTotal(pool.getMaxActive());
        	// 最大建立连接等待时间
            jedisPoolConfig.setMaxWaitMillis(pool.getMaxWait());
            //在空闲时检查有效性, 默认false 
            jedisPoolConfig.setTestWhileIdle(true);
        }
        return jedisPoolConfig;
    }
	
	
	@Bean
	public RedissonClient redissonClient(RedisProperties redisProperties) {
		Config config = new Config();
		String address = "redis://" + redisProperties.getHost() + ":" + redisProperties.getPort();
		SingleServerConfig serverConfig = config.useSingleServer();
		serverConfig.setAddress(address);
		if(!StringUtils.isEmpty(redisProperties.getPassword())){
			serverConfig.setPassword(redisProperties.getPassword());
		}
		serverConfig.setDatabase(redisProperties.getDatabase());
		
		return Redisson.create(config);
	}
	
	@PreDestroy
	public void destory(){
		RedisLockUtil.destory();
	}

}
