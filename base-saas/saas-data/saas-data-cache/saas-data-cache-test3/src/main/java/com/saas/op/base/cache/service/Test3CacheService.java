package com.saas.op.base.cache.service;

import com.saas.op.base.cache.entity.Test3Cache;

/**

 */

public interface Test3CacheService {


    /**
     * 缓存

     */
    Test3Cache cacheTest3(Test3Cache test3Cache);


    /**
     * 刷新
     */
    Test3Cache refreshTest3(Test3Cache test3Cache);

    /**
     * 获取
     */
    Test3Cache getTest3(Test3Cache test3Cache);


    /**
     * 删除

     */
    Test3Cache deleteTest3(Test3Cache test3Cache);


}
