package com.saas.op.base.cache.service;

import com.saas.op.base.cache.entity.ServicedemoCache;

/**

 */

public interface ServicedemoCacheService {


    /**
     * 缓存

     */
    ServicedemoCache cacheServicedemo(ServicedemoCache servicedemoCache);


    /**
     * 刷新
     */
    ServicedemoCache refreshServicedemo(ServicedemoCache servicedemoCache);

    /**
     * 获取
     */
    ServicedemoCache getServicedemo(ServicedemoCache servicedemoCache);


    /**
     * 删除

     */
    ServicedemoCache deleteServicedemo(ServicedemoCache servicedemoCache);


}
