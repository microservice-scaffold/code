package com.saas.op.base.cache.entity;

import java.io.Serializable;
import java.util.Date;

/**
 **/
public class Test2Cache implements Serializable {

	private static final long serialVersionUID = 5411837695569814746L;

	private Integer id;
	
    private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
