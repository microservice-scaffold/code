package com.saas.op.base.cache.service;

import com.saas.op.base.cache.entity.Test2Cache;

/**

 */

public interface Test2CacheService {


    /**
     * 缓存

     */
    Test2Cache cacheTest2(Test2Cache test2Cache);


    /**
     * 刷新
     */
    Test2Cache refreshTest2(Test2Cache test2Cache);

    /**
     * 获取
     */
    Test2Cache getTest2(Test2Cache test2Cache);


    /**
     * 删除

     */
    Test2Cache deleteTest2(Test2Cache test2Cache);


}
