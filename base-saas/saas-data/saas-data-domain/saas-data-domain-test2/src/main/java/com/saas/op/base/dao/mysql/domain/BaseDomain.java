package com.saas.op.base.dao.mysql.domain;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

public class BaseDomain {
	
	public static final int MAX_PAGE_SIZE = 10;

	/**
	 * 分页的起始位置
	 */
	@Transient
	private Integer start;

	/**
	 * 每页最大数
	 */
	@Transient
	private Integer pageSize;

	@Transient
	public Map<String, Object> params = new HashMap<>();

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public void addParam(String key, Object value) {
		params.put(key, value);
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

}
