package com.saas.data.ignite.configure;

import java.sql.Types;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.ignite.cache.store.jdbc.CacheJdbcPojoStoreFactory;
import org.apache.ignite.cache.store.jdbc.JdbcType;
import org.apache.ignite.cache.store.jdbc.JdbcTypeField;
import org.apache.ignite.cache.store.jdbc.dialect.MySQLDialect;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.saas.data.base.ignite.config.EnableIgniteRepositories;
import com.saas.data.ignite.model.Contact;
import com.saas.data.ignite.model.ContactType;
import com.saas.data.ignite.model.Gender;
import com.saas.data.ignite.model.Person;

@Configuration
@EnableIgniteRepositories("com.saas")
public class IgniteConfigure {

	@Bean
	public IgniteConfiguration igniteConfiguration(DataSource datasource) {
		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setIgniteInstanceName("ignite-1");
		cfg.setPeerClassLoadingEnabled(true);

		CacheConfiguration<Long, Contact> contactCfg = contactCacheConfiguration(datasource);
		CacheConfiguration<Long, Person> personCfg = personCacheConfiguration(datasource);

		cfg.setCacheConfiguration(contactCfg, personCfg);
		
		//return Ignition.start(cfg);
		return cfg;
	}
	
	public CacheConfiguration<Long, Contact> contactCacheConfiguration(DataSource datasource){
		CacheConfiguration<Long, Contact> ccfg = new CacheConfiguration<>("ContactCache");
		ccfg.setIndexedTypes(Long.class, Contact.class);
		ccfg.setWriteBehindEnabled(true);
		ccfg.setReadThrough(true);
		ccfg.setWriteThrough(true);
		CacheJdbcPojoStoreFactory<Long, Contact> cFactory = new CacheJdbcPojoStoreFactory<>();
		cFactory.setDataSource(datasource);
		cFactory.setDialect(new MySQLDialect());
		JdbcType jdbcContactType = new JdbcType();
		jdbcContactType.setCacheName("ContactCache");
		jdbcContactType.setKeyType(Long.class);
		jdbcContactType.setValueType(Contact.class);
		jdbcContactType.setDatabaseTable("contact");
		jdbcContactType.setDatabaseSchema("ignite");
		jdbcContactType.setKeyFields(new JdbcTypeField(Types.INTEGER, "id", Long.class, "id"));
		jdbcContactType.setValueFields(new JdbcTypeField(Types.VARCHAR, "contact_type", ContactType.class, "type"),
				new JdbcTypeField(Types.VARCHAR, "location", String.class, "location"),
				new JdbcTypeField(Types.INTEGER, "person_id", Long.class, "personId"));
		cFactory.setTypes(jdbcContactType);
		ccfg.setCacheStoreFactory(cFactory);
		return ccfg;
	}
	
	
	public CacheConfiguration<Long, Person> personCacheConfiguration(DataSource datasource){
		CacheConfiguration<Long, Person> ccfg = new CacheConfiguration<>("PersonCache");
		ccfg.setIndexedTypes(Long.class, Person.class);
		ccfg.setWriteBehindEnabled(true);
		ccfg.setReadThrough(true);
		ccfg.setWriteThrough(true);
		CacheJdbcPojoStoreFactory<Long, Person> cFactory = new CacheJdbcPojoStoreFactory<>();
		cFactory.setDataSource(datasource);
		cFactory.setDialect(new MySQLDialect());
		JdbcType jdbcType = new JdbcType();
		jdbcType.setCacheName("PersonCache");
		jdbcType.setKeyType(Long.class);
		jdbcType.setValueType(Person.class);
		jdbcType.setDatabaseTable("person");
		jdbcType.setDatabaseSchema("ignite");
		jdbcType.setKeyFields(new JdbcTypeField(Types.INTEGER, "id", Long.class, "id"));
		jdbcType.setValueFields(new JdbcTypeField(Types.VARCHAR, "first_name", String.class, "firstName"),
				new JdbcTypeField(Types.VARCHAR, "last_name", String.class, "lastName"),
				new JdbcTypeField(Types.VARCHAR, "gender", Gender.class, "gender"),
				new JdbcTypeField(Types.VARCHAR, "country", String.class, "country"),
				new JdbcTypeField(Types.VARCHAR, "city", String.class, "city"),
				new JdbcTypeField(Types.VARCHAR, "address", String.class, "address"),
				new JdbcTypeField(Types.DATE, "birth_date", Date.class, "birthDate"));
		cFactory.setTypes(jdbcType);
		ccfg.setCacheStoreFactory(cFactory);
		return ccfg;
	}
	
}
