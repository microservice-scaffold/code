package com.saas.data.ignite.model;

public enum ContactType {

	EMAIL, MOBILE, PHONE, SKYPE;
}
