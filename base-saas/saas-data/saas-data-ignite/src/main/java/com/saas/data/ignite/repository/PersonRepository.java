package com.saas.data.ignite.repository;

import java.util.List;

import com.saas.data.base.ignite.IgniteRepository;
import com.saas.data.base.ignite.config.Query;
import com.saas.data.base.ignite.config.RepositoryConfig;
import com.saas.data.ignite.model.Contact;
import com.saas.data.ignite.model.Person;

@RepositoryConfig(cacheName = "PersonCache")
public interface PersonRepository extends IgniteRepository<Person, Long> {
	
	List<Person> findByFirstNameAndLastName(String firstName, String lastName);
	
	@Query("SELECT c.* FROM Person p JOIN \"ContactCache\".Contact c ON p.id=c.personId WHERE p.firstName=? and p.lastName=?")
	List<Contact> selectContacts(String firstName, String lastName);
	
	@Query("SELECT p.id, p.firstName, p.lastName, c.id, c.type, c.location FROM Person p JOIN \"ContactCache\".Contact c ON p.id=c.personId WHERE p.firstName=? and p.lastName=?")
	List<List<?>> selectContacts2(String firstName, String lastName);
}
