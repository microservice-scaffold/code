package com.saas.data.ignite.repository;

import java.util.List;

import com.saas.data.base.ignite.IgniteRepository;
import com.saas.data.base.ignite.config.RepositoryConfig;
import com.saas.data.ignite.model.Contact;

@RepositoryConfig(cacheName = "ContactCache")
public interface ContactRepository extends IgniteRepository<Contact, Long> {

	List<Contact> findByLocation(String location);
	
}
