package com.saas.data.base.page;

import lombok.Data;

@Data
public class PageData<T> {
	
	private static final int FIRST = 1;

	int totalPage;

	long totalSize;
	
	T datas;
	
	public PageData(long totalSize,T datas,int pageSize){
		if(totalSize <= pageSize){
			this.totalPage = FIRST;
		}else{
			this.totalPage = (int)((totalSize - 1)/pageSize + 1);
		}
		this.datas = datas;
		this.totalSize = totalSize;
	}
}
