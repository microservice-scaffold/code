
package com.saas.data.base.ignite.config;

import java.util.Collection;
import java.util.Collections;

import org.springframework.data.repository.config.RepositoryConfigurationExtension;
import org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport;

import com.saas.data.base.ignite.IgniteRepository;
import com.saas.data.base.ignite.support.IgniteRepositoryFactoryBean;

/**
 * Apache Ignite specific implementation of
 * {@link RepositoryConfigurationExtension}.
 */
public class IgniteRepositoryConfigurationExtension extends RepositoryConfigurationExtensionSupport {
	/** {@inheritDoc} */
	@Override
	public String getModuleName() {
		return "Apache Ignite";
	}

	/** {@inheritDoc} */
	@Override
	protected String getModulePrefix() {
		return "ignite";
	}

	/** {@inheritDoc} */
	@Override
	public String getRepositoryFactoryClassName() {
		return IgniteRepositoryFactoryBean.class.getName();
	}

	/** {@inheritDoc} */
	@Override
	protected Collection<Class<?>> getIdentifyingTypes() {
		return Collections.<Class<?>>singleton(IgniteRepository.class);
	}
}
