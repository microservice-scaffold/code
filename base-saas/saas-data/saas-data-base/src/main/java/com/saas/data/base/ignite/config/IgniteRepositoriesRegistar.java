
package com.saas.data.base.ignite.config;

import java.lang.annotation.Annotation;

import org.springframework.data.repository.config.RepositoryBeanDefinitionRegistrarSupport;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;

/**
 * Apache Ignite specific implementation of {@link RepositoryBeanDefinitionRegistrarSupport}.
 */
public class IgniteRepositoriesRegistar extends RepositoryBeanDefinitionRegistrarSupport {
    
	/** {@inheritDoc} */
    @Override 
    protected Class<? extends Annotation> getAnnotation() {
        return EnableIgniteRepositories.class;
    }

    /** {@inheritDoc} */
    @Override 
    protected RepositoryConfigurationExtension getExtension() {
        return new IgniteRepositoryConfigurationExtension();
    }
}
