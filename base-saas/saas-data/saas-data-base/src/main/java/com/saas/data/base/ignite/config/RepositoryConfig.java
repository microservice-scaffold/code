

package com.saas.data.base.ignite.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation can be used to pass Ignite specific parameters to a bound repository.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RepositoryConfig {
    /**
     * @return A name of a distributed Apache Ignite cache an annotated repository will be mapped to.
     */
    String cacheName() default "";
}