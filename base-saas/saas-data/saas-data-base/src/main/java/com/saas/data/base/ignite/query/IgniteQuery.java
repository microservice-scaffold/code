

package com.saas.data.base.ignite.query;

/**
 * Ignite query helper class. For internal use only.
 */
public class IgniteQuery {
    /** */
    enum Option {
        /** Query will be used with Sort object. */
        SORTING,

        /** Query will be used with Pageable object. */
        PAGINATION,

        /** No advanced option. */
        NONE
    }

    /** Sql query text string. */
    private final String sql;

    /** */
    private final boolean isFieldQuery;

    /** Type of option. */
    private final Option option;

    /**
     * @param sql Sql.
     * @param isFieldQuery Is field query.
     * @param option Option.
     */
    public IgniteQuery(String sql, boolean isFieldQuery, Option option) {
        this.sql = sql;
        this.isFieldQuery = isFieldQuery;
        this.option = option;
    }

    /**
     * Text string of the query.
     *
     * @return SQL query text string.
     */
    public String sql() {
        return sql;
    }

    /**
     * Returns {@code true} if it's Ignite SQL fields query, {@code false} otherwise.
     *
     * @return {@code true} if it's Ignite SQL fields query, {@code false} otherwise.
     */
    public boolean isFieldQuery() {
        return isFieldQuery;
    }

    /**
     * Advanced querying option.
     *
     * @return querying option.
     */
    public Option options() {
        return option;
    }
}

