package com.saas.data.base.page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import lombok.Data;

@Data
public class Pagination<T> {
	
	private static final int DEFAULT_PAGE_INDEX = 1;
	
	private static final int DEFAULT_PAGE_SIZE = 10;
    
    //排序条件
    private Sort sort;
    
    private Pageable pageable;
    
    private T params;
    
    public Pagination(){
    	this.pageable = new PageRequest(DEFAULT_PAGE_INDEX, DEFAULT_PAGE_SIZE);
    }
    
    public Pagination(Integer pageSize){
    	this.pageable = new PageRequest(DEFAULT_PAGE_INDEX, pageSize);
    }
    
    public Pagination(Integer pageIndex,Integer pageSize){
    	this.pageable = new PageRequest(pageIndex - 1, pageSize);
    }

    public void addSort(Direction direction, String... fields){
    	this.sort = new Sort(direction, fields);
    }
}
