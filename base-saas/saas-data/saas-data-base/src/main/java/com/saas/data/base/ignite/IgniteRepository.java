
package com.saas.data.base.ignite;

import java.io.Serializable;
import java.util.Map;
import org.springframework.data.repository.CrudRepository;

/**
 * Apache Ignite repository that extends basic capabilities of {@link CrudRepository}.
 */
public interface IgniteRepository<T, ID extends Serializable> extends CrudRepository<T, ID> {
    /**
     * Saves a given entity using provided key.
     * </p>
     * It's suggested to use this method instead of default {@link CrudRepository#save(Object)} that generates
     * IDs (keys) that are not unique cluster wide.
     *
     * @param key Entity's key.
     * @param entity Entity to save.
     * @param <S> Entity type.
     * @return Saved entity.
     */
    <S extends T> S save(ID key, S entity);

    /**
     * Saves all given keys and entities combinations.
     * </p>
     * It's suggested to use this method instead of default {@link CrudRepository#save(Iterable)} that generates
     * IDs (keys) that are not unique cluster wide.
     *
     * @param entities Map of key-entities pairs to save.
     * @param <S> type of entities.
     * @return Saved entities.
     */
    <S extends T> Iterable<S> save(Map<ID, S> entities);

    /**
     * Deletes all the entities for the provided ids.
     *
     * @param ids List of ids to delete.
     */
    void deleteAll(Iterable<ID> ids);
}
