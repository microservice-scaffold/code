package com.saas.data.base.mysql;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.fastjson.JSON;

@Configuration
@EnableConfigurationProperties(DruidProperties.class)
public class DruidAutoConfigure {

	private Logger logger = LoggerFactory.getLogger(DruidAutoConfigure.class);

	@Bean
	//@Primary
	public DataSource druidDataSource(DruidProperties druidProperties) {
		logger.debug("======================DruidProperties=========================");
		logger.debug("====DruidProperties：{}", JSON.toJSONString(druidProperties));
		logger.debug("======================DruidProperties=========================");
		DruidDataSource datasource = new DruidDataSource();

		datasource.setUrl(druidProperties.getUrl());
		datasource.setUsername(druidProperties.getUsername());
		datasource.setPassword(druidProperties.getPassword());
		datasource.setDriverClassName(druidProperties.getDriverClassName());
		datasource.setInitialSize(druidProperties.getInitialSize());
		datasource.setMinIdle(druidProperties.getMinIdle());
		datasource.setMaxActive(druidProperties.getMaxActive());
		datasource.setMaxWait(druidProperties.getMaxWait());
		datasource.setTimeBetweenEvictionRunsMillis(druidProperties.getTimeBetweenEvictionRunsMillis());
		datasource.setMinEvictableIdleTimeMillis(druidProperties.getMinEvictableIdleTimeMillis());
		datasource.setValidationQuery(druidProperties.getValidationQuery());
		datasource.setTestWhileIdle(druidProperties.isTestWhileIdle());
		datasource.setTestOnBorrow(druidProperties.isTestOnBorrow());
		datasource.setTestOnReturn(druidProperties.isTestOnReturn());
		datasource.setPoolPreparedStatements(druidProperties.isPoolPreparedStatements());
		try {
			datasource.setFilters(druidProperties.getFilters());
		} catch (Exception e) {
			logger.error("druid configuration initialization filter", e);
		}
		return datasource;
	}
	
	@Bean
	public ServletRegistrationBean druidServlet(DruidProperties druidProperties) {
		ServletRegistrationBean reg = new ServletRegistrationBean();
		reg.setServlet(new StatViewServlet());
		reg.addUrlMappings("/druid/*");
		reg.addInitParameter("loginUsername", druidProperties.getUsername());
		reg.addInitParameter("loginPassword", druidProperties.getPassword());
		return reg;
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		filterRegistrationBean.addInitParameter("profileEnable", "true");
		filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
		filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
		return filterRegistrationBean;
	}

	
}
