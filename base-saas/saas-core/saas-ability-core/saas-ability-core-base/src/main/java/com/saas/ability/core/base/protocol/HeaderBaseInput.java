package com.saas.ability.core.base.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * 头部信息input，用于新接口协议
 *
 */
@Data
public class HeaderBaseInput implements Serializable {


    private static final long serialVersionUID = -1159223597593080626L;

    /**
     * 应用ID
     */
    private Integer fromType;

    /**
     * 时间戳
     */
    private Long timesTamp;

    /**
     * 设备编码
     */
    private String deviceSn;


    /**
     * 无人设备id
     */
    private Integer deviceId;

    /**
     * 终端版本号
     */
    private String appVersion;

    /**
     * 设备版本号
     */
    private String deviceVersion;

    /**
     * 设备型号
     */
    private String deviceModel;


    /**
     * 安卓版本号
     */
    private String androidVersion;

    /**
     * 请求接口的版本号
     */
    private String apiVersion;

    /**
     * 登录令牌
     */
    private String accessToken;

    /**
     * 签名内容
     */
    private Integer signature;

    /**
     * 6：无人
     */
    private Integer deviceType;

    /**
     * 终端类型 1:安卓 2：ios
     */
    private Integer terminalType;

    /**
     * 系统类型

     */
    private Integer systemType;
}