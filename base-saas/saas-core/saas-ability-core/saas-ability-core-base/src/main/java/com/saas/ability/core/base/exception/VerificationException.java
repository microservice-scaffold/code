package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

/**
 * 通用验证异常
 *
 */
public class VerificationException extends BaseException  {

	private static final long serialVersionUID = 370378930928155031L;

	
	public VerificationException(){
		super(RetCode.VERIFY_INVALID.retCode,RetCode.VERIFY_INVALID.message);
	}
	
	public VerificationException(String message){
		super(RetCode.VERIFY_INVALID.retCode,message);
	}
	
	public VerificationException(String retCode,String message){
		super(retCode,message);
	}
}
