package com.saas.ability.core.base.exception;

/**
 * 满足2.0前端的异常
 *
 */
public class RespException extends RuntimeException {
	
	private static final long serialVersionUID = 3944156971920024617L;

	 /**
     * 请求成功
     */
    public static final String SUCCESS = "1";
    
    /**
     * 请求失败
     */
    public static final String FAIL = "-1";

    /**
     * token失效
     */
    public static final String TOKEN_INVALID = "-4";

    /**
     * 结果状态
     */
    private String status;
    
    /**
     * 提示信息
     */
    private String info;

    
    public RespException(String status,String info){
    	super(info);
		this.status = status;
		this.info = info;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getInfo() {
		return info;
	}


	public void setInfo(String info) {
		this.info = info;
	}
    
    
}
