package com.saas.ability.core.base.protocol;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class PageOutput<T> extends BaseOutput<List<T>> {
	private static final long serialVersionUID = 2760046387235784535L;
	//当前页
	private Integer pageNo;
	//每页显示记录条数
	private Integer pageSize;
	//总页数
	private Integer totalPages;
	//总记录数
	private Long totalRows;
	
	public PageOutput() {
		
	}
	public PageOutput(String retCode, String retMsg) {
		super(retCode,retMsg);
	}
	public PageOutput(String retCode, String retMsg, Integer pageNo, Integer pageSize, Long totalRows, List<T> retData){
		super(retCode,retMsg,retData);
		this.pageNo=pageNo;
		this.pageSize=pageSize;
		this.totalRows=totalRows;
		this.totalPages=(int) ((totalRows%pageSize==0)?totalRows/pageSize:(totalRows/pageSize)+1);
	}
	public PageOutput(String retCode, String retMsg, Integer pageNo, Integer pageSize, Long totalRows, Integer pages, List<T> retData){
		super(retCode,retMsg,retData);
		this.pageNo=pageNo;
		this.pageSize=pageSize;
		this.totalRows=totalRows;
		this.totalPages=pages;
	}
}
