package com.saas.ability.core.base.protocol;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthBaseInput implements Serializable {

	private static final long serialVersionUID = -3960267923966015182L;

	/**
	 * 操作员ID
	 */
	private Integer operatorId;
	
	/**
	 * 操作员名称
	 */
	private String operator;

	/**
	 * 设备类型
	 */
	private Integer deviceType;

	/**
	 * 品牌id
	 */
	private Integer groupId;

	/**
	 * 商户id
	 */
	private Integer companyId;
	/**
	 * 门店id
	 */
	private Integer resId;

	/**
	 * 根据system_type 类型放置不同的id
	 * systemType = 19时 为前置仓id
	 */
	private Integer systemKeyId;

	/**
	 * 系统类型
	 */
	private Integer systemType;
	
}
