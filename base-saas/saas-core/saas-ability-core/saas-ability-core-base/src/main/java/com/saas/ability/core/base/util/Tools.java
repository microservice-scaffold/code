package com.saas.ability.core.base.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 描述: 处理共用工具
 */
public class Tools {

    /**
     * 将用对象转换成BigDecimal
     * @param value
     * @return
     */
    public static BigDecimal getBigDecimal(Object value ) {
        BigDecimal ret = null;
        if( value != null ) {
            if( value instanceof BigDecimal ) {
                ret = (BigDecimal) value;
            } else if( value instanceof String ) {
                ret = new BigDecimal( (String) value );
            } else if( value instanceof BigInteger) {
                ret = new BigDecimal( (BigInteger) value );
            } else if( value instanceof Number ) {
                ret = new BigDecimal( ((Number)value).doubleValue() );
            } else {
                throw new ClassCastException("Not possible to coerce ["+value+"] from class "+value.getClass()+" into a BigDecimal.");
            }
        }
        return ret;
    }

    /**
     * 将对象转换成int
     * @param key
     * @return
     */
    public static int getInt(Object key){
        Object value = key;
        if(value instanceof String){
            return Integer.parseInt(value.toString());
        }else if(value instanceof Integer){
            return ((Integer) value).intValue();
        }else {
            return 0;
        }
    }


    /**
     * 当前日期减指定的天数
     * @param day
     * @return
     */
    public static String currentDateSub(int day){
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) - day);
        return dft.format(date.getTime());
    }

}
