package com.saas.ability.core.base.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EventPublisher {

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;
	
	public <T> void publish(T source){
		applicationEventPublisher.publishEvent(new MessageEvent<>(source));
	}
}
