package com.saas.ability.core.base.thread;

import org.springframework.core.NamedThreadLocal;

/**
 * 存储本地线程
 *
 */
public class LocalThreadCache<T> {

    private static final ThreadLocal localThreadCache = new NamedThreadLocal<>("LocalThreadCache");

    /**
     * 设置
     *
     * @param obj
     */
    public final static <T> void setCache(T obj) {
        localThreadCache.set(obj);
    }

    /**
     * 获取
     *
     * @return
     */
    public final static <T> T getCache() {
        return (T) localThreadCache.get();
    }

    /**
     * 移除
     */
    public final static void remove() {
        localThreadCache.remove();
    }
}
