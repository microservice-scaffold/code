

package com.saas.ability.core.base.thread;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

public class ThreadPoolManager {

    ExecutorFactory executorFactory;

    private final Map<String, Executor> pools = new ConcurrentHashMap<>();
    
    
    public ThreadPoolManager(ThreadPoolConfig config){
    	executorFactory = new SimpleExecutorFactory(config);
    }

    public Executor getEventBusExecutor() {
        return pools.computeIfAbsent("common-event-bus-executor", 
        		s -> executorFactory.get(ExecutorFactory.EVENT_BUS,null));
    }
    
    public Executor getExecutor(String name) {
        return pools.computeIfAbsent(name + "-event-bus-executor", 
        		s -> executorFactory.get(name + "-" + ExecutorFactory.EVENT_BUS,null));
    }
    
    public Executor getScheduledExecutor(String name) {
        return pools.computeIfAbsent(name + "-scheduled-executor", 
        		s -> executorFactory.get(name,ExecutorFactory.EXECUTOR_TYPE));
    }

    public void register(String name, Executor executor) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(executor);
        pools.put(name, executor);
    }

    public Map<String, Executor> getActivePools() {
        return pools;
    }

}
