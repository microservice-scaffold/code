package com.saas.ability.core.base.util;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import com.saas.ability.core.base.config.AliyunossProperties;
import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/** 

 */
@Component
public class UploadUtil {
	
	@Autowired(required=false)
	AliyunossProperties aliyunossProperties;
	
	@Autowired(required=false)
	OSSClient client;
	
	//上传文件
    @Deprecated
    public String putObject(String key, InputStream content)  {
        try {

            // 创建上传Object的Metadata
            ObjectMetadata meta = new ObjectMetadata();

            // 必须设置ContentLength
            meta.setContentLength(content.available());
            // 上传Object.
            PutObjectResult result = client.putObject(aliyunossProperties.getBucketName(), key, content, meta);

            // 打印ETag
            if (StringUtils.isNoneBlank(result.getETag())) {
                return aliyunossProperties.getMappoint1() + key;
            } else {
                return null;
            }
        }catch (Exception e){
            throw new ServiceException(RetCode.HANDLE_FAIL.retCode,"上传失败!");
        }

    }

    /**
     * 上传至阿里云并返回访问地址
     * @param key
     * @param content
     * @return
     */
    public String putObjectReturnUrl(String key, InputStream content)  {
        try {

            // 创建上传Object的Metadata
            ObjectMetadata meta = new ObjectMetadata();

            // 必须设置ContentLength
            meta.setContentLength(content.available());
            // 上传Object.
            PutObjectResult result = client.putObject(aliyunossProperties.getBucketName(), key, content, meta);

            // 打印ETag
            if (StringUtils.isBlank(result.getETag())) {
                key = null;
            }
            return this.getUrl(key);
        }catch (Exception e){
            throw new ServiceException(RetCode.HANDLE_FAIL.retCode,"上传失败!");
        }
    }

    /**
     * 获得url链接
     *
     * @param key
     * @return
     */
    public String getUrl(String key) {
        // 生成URL
        if (StringUtils.isNotBlank(key)){
            return aliyunossProperties.getMappoint1()+key;
        }
        return null;
    }


    /**
     * 返回上传路径
     * @param inputStreams 输入流
     * @param urlType 上传类型 1 : 订单评价 2:采购退货
     * @param billId bill_id 单号
     * @return
     * @throws Exception
     */
    public List<String> getUrl(List<InputStream> inputStreams, Integer urlType, Integer billId){
        List<String> uploadPathList = new ArrayList<>();
        String path = "";
        String fileName = "";
        if(null != inputStreams && inputStreams.size() > 0){
            for(int i = 0;i < inputStreams.size();i++){
                if(urlType == 1){
                    fileName = aliyunossProperties.getPreOrderEvaluateKey() + billId + "_" +System.currentTimeMillis() + "_" + i +".jpg";
                }else if(urlType == 2){
                    fileName = aliyunossProperties.getPrePurOrderKey() + billId + "_" +System.currentTimeMillis()+ "_" + i +".jpg";
                }
                path = putObject(fileName,inputStreams.get(i));
                if(null != path && StringUtils.isNotEmpty(path)){
                    uploadPathList.add(aliyunossProperties.getMappoint1() + fileName);
                }else{
                    uploadPathList.add(aliyunossProperties.getMappoint1() + aliyunossProperties.getPreSupplierKey() + "initial.jpg");
                }
            }

        }
        return uploadPathList;
    }
}
