package com.saas.ability.core.base.exception;

/**
 * 签名和验签异常
 *
 */
public class SignException extends RuntimeException {
	
	private static final long serialVersionUID = 3064087446538292567L;

	protected String retCode;
	
	protected String message;
	
	public SignException(String retCode,String message){
		this.retCode = retCode;
		this.message = message;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
