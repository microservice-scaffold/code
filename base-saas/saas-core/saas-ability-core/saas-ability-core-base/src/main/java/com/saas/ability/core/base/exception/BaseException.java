package com.saas.ability.core.base.exception;

/**
 * 通用业务异常
 *
 */
public class BaseException extends RuntimeException {
	
	private static final long serialVersionUID = 3944156971920024617L;

	protected String retCode;
	
	protected String message;
	
	public BaseException(String retCode,String message){
		this.retCode = retCode;
		this.message = message;
	}
	
	public BaseException(String message,Throwable cause){
		super(message,cause);
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
