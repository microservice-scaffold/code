package com.saas.ability.core.base.protocol;

import com.saas.ability.core.base.util.UUIDGenerator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @description: mqtt信息发布输入参数
 */
@ToString
public class MqttPublishInput<T> implements Serializable {

    /**
     * 唯一标识(UUID)，消息id
     */
    private String messageId = UUIDGenerator.getUUID();

    /**
     * 发布的topic
     */
    private String topic;

    /**
     * MqttConstant.TargetType
     */
    private Integer targetType;

    /**
     * targetType = 1 时 RobotTaskConstant.TaskType
     * targetType = 2 时 MqttConstant.DevicePublishType
     * targetType = 3 时 MqttConstant.WxpPublishType
     */
    private Integer taskType;

    /**
     * 是否保存信息到mqtt服务器,默认false
     */
    private boolean isRetained = false;

    /**
     * 设备sn
     */
    private String deviceSn;

    /**
     * 发布时间
     */
    // @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private long publishDate;

//    /**
//     * 是否云平台发送给客户端， 默认是
//     * 前端发送给云平台时需要将此参数设置为false
//     */
//    private boolean isSendToClient = true;

    /**
     * 业务参数
     */
    private T params;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Integer getTargetType() {
		return targetType;
	}

	public void setTargetType(Integer targetType) {
		this.targetType = targetType;
	}

	public Integer getTaskType() {
		return taskType;
	}

	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}

	public boolean isRetained() {
		return isRetained;
	}

	public void setRetained(boolean isRetained) {
		this.isRetained = isRetained;
	}

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	public long getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(long publishDate) {
		this.publishDate = publishDate;
	}

	public T getParams() {
		return params;
	}

	public void setParams(T params) {
		this.params = params;
	}

}
