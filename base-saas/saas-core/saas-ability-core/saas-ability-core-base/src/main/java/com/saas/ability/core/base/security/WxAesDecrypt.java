package com.saas.ability.core.base.security;

/**
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.Security;

public class WxAesDecrypt {

    private static final String WATERMARK = "watermark";
    private static final String APPID = "appid";

    private static boolean hasInited = false;

    public static void init() {
        if (hasInited) {
            return;
        }
        Security.addProvider(new BouncyCastleProvider());
        hasInited = true;
    }

    public static String decrypt(String session_key, String iv, String encryptData) {

        String decryptString = "";
        init();
        byte[] sessionKeyByte = Base64.decodeBase64(session_key);
        byte[] ivByte = Base64.decodeBase64(iv);
        byte[] encryptDataByte = Base64.decodeBase64(encryptData);
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            Key key = new SecretKeySpec(sessionKeyByte, "AES");
            AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance("AES");
            algorithmParameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, key, algorithmParameters);
            byte[] bytes = cipher.doFinal(encryptDataByte);
            decryptString = new String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptString;
    }

    public static void main(String[] args) {
        String appId = "saas";
        String session_key = "saas";
        String iv = "saas";
        String encryptData =
                "saas";
        String decrypt = decrypt(session_key, iv, encryptData);
        JSONObject jsonObject = JSON.parseObject(decrypt);
        String decryptAppid = jsonObject.getJSONObject(WATERMARK).getString(APPID);
        if(!appId.equals(decryptAppid)){
            decrypt = "";
        }
        System.out.println(decrypt);
    }
}
