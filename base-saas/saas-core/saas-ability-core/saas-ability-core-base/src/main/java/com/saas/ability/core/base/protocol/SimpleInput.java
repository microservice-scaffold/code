package com.saas.ability.core.base.protocol;

import lombok.Data;

/**
 * 通用简单input
 *
 */
@Data
public class SimpleInput<T> {

    private static final long serialVersionUID = -3487911165917725044L;

    T data;

}
