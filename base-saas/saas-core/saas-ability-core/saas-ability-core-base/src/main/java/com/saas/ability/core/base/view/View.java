package com.saas.ability.core.base.view;

import java.util.Map;

/**
 * 模版视图
 *
 */
public interface View {

	/**
	 * 完成视图构建
	 * @param dataModel
	 * @return
	 * @throws Exception
	 */
	String render(Map<String,Object> dataModel);

	/**
	 * view最近一次被创建的时间
	 * @return
	 */
	Long getLastTime();
}
