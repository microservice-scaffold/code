package com.saas.ability.core.base.util;

import java.util.regex.Pattern;

public class HttpUtil {
	
	/**
	 * 允许IP和DOMAIN（域名） 
	 */
	private static final String URLREGX ="^((https|http)?://)(([0-9]{1,3}\\.){3}[0-9]{1,3}|([\\S]+))";

	public static boolean isUrl(String url){
		return Pattern.matches(URLREGX ,url );
	}
	
}
