package com.saas.ability.core.base.config;

public class TemplateConfig {
	
	private String resourceLoadPath;
	
	private int cacheLimit;

	public String getResourceLoadPath() {
		return resourceLoadPath;
	}

	public void setResourceLoadPath(String resourceLoadPath) {
		this.resourceLoadPath = resourceLoadPath;
	}

	public int getCacheLimit() {
		return cacheLimit;
	}

	public void setCacheLimit(int cacheLimit) {
		this.cacheLimit = cacheLimit;
	}
	
	
}

