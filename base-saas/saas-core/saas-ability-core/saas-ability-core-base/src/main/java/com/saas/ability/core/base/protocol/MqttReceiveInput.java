package com.saas.ability.core.base.protocol;

import com.saas.ability.core.base.constant.retcode.RetCode;
import com.saas.ability.core.base.exception.ServiceException;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * mqtt回调输入参数
 *
 */
@Data
public class MqttReceiveInput<T> implements Serializable {

    /**
     * UUID，作为消息的唯一标识
     */
    private String messageId;

    /**
     * 发布的topic
     */
    private String topic;

    /**
     * MqttConstant.MqttReceiveBoardCardMsgType
     */
    private Integer msgType;

    /**
     * 设备类型 DeviceConstant.DeviceType
     */
    private Integer deviceType;

    /**
     * 设备sn
     */
    private String deviceSn;

    /**
     * 终端类型 1:android 2:ios, 默认1
     */
    private Integer terminalType = 1;

    /**
     * 设备型号
     */
    private String deviceVersion;

    /**
     * APP版本号
     */
    private String appVersion;

    /**
     * 后台接口API的版本号
     */
    private String apiVersion;

    /**
     * 系统类型
     */
    private Integer systemType;

    /**
     * 业务参数
     */
    private T params;

    /**
     * 回调时间
     */
    private long receiveDate;

    /**
     * 访问服务的url
     */
    private String url;

    /**
     * 添加到redis的到期时间
     */
    private long expire;

    /**
     * 检查参数
     */
    public void checkParam() {
        if (StringUtils.isEmpty(this.messageId)) {
            throw new ServiceException(RetCode.PARAM_INVALID.retCode, "处理接收到的mqtt消息时,messageId不能为空");
        }
        if (StringUtils.isEmpty(this.topic)) {
            throw new ServiceException(RetCode.PARAM_INVALID.retCode, "处理接收到的mqtt消息时,topic不能为空");
        }
        if (StringUtils.isEmpty(this.msgType)) {
            throw new ServiceException(RetCode.PARAM_INVALID.retCode, "处理接收到的mqtt消息时,msgType不能为空");
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"messageId\":\"")
                .append(messageId).append('\"');
        sb.append(",\"topic\":\"")
                .append(topic).append('\"');
        sb.append(",\"msgType\":")
                .append(msgType);
        sb.append(",\"deviceSn\":\"")
                .append(deviceSn).append('\"');
        sb.append(",\"terminalType\":")
                .append(terminalType);
        sb.append(",\"deviceVersion\":\"")
                .append(deviceVersion).append('\"');
        sb.append(",\"apiVersion\":\"")
                .append(apiVersion).append('\"');
        sb.append(",\"systemType\":")
                .append(systemType);
        sb.append(",\"params\":")
                .append(params);
        sb.append(",\"receiveDate\":")
                .append(receiveDate);
        sb.append(",\"url\":\"")
                .append(url).append('\"');
        sb.append(",\"expire\":")
                .append(expire);
        sb.append('}');
        return sb.toString();
    }
}