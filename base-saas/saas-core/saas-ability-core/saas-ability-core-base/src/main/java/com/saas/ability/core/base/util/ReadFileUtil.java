

package com.saas.ability.core.base.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ReadFileUtil {
  
  /**
   * buff:(读取文件). <br/>

   */
  public static String buff(String pathName) {
    String fullFileName = pathName;

    File file = new File(fullFileName);
    Scanner scanner = null;
    StringBuilder buffer = new StringBuilder();
    try {
      scanner = new Scanner(file, "utf-8");
      while (scanner.hasNextLine()) {
        buffer.append(scanner.nextLine());
      }

    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block

    } finally {
      if (scanner != null) {
        scanner.close();
      }
    }

    return buffer.toString();
  }

}

