package com.saas.ability.core.base.http;

import java.net.URI;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetEndpoint {
	
	static  Logger logger  = LoggerFactory.getLogger(GetEndpoint.class);

	public static HttpRequestBase getHttpRequestBase(String url) {
		return getHttpGet(url,null);
	}

	public static HttpGet getHttpGet(String url, Map<String,String> params) {
		//CloseableHttpClient httpClient = connManager.getHttpClient();
		try{
			URIBuilder builder = new URIBuilder(url);	
			if(params != null && !params.isEmpty()){
				for (String key : params.keySet()) {
					builder.addParameter(key, params.get(key));
				}
			}
			URI uri = builder.build();	
			return new HttpGet(uri);
		}catch(Exception e){
			logger.error("生成url地址异常", e);
			throw new RuntimeException("生成url地址异常", e);
		}
	}
}
