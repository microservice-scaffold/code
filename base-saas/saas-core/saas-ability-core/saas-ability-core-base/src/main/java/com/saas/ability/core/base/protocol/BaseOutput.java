package com.saas.ability.core.base.protocol;

import java.io.Serializable;

import com.saas.ability.core.base.constant.retcode.RetCode;

/**
 * 接口输出
 *
 */
public class BaseOutput<T> implements Serializable {

	private static final long serialVersionUID = -7553340687631250764L;

	protected String retCode;

	protected String retMsg;

	protected T retData;
	
	public BaseOutput(){
	}
	
	public BaseOutput(T retData){
		this.retCode = RetCode.SUCCESS.retCode;
		this.retMsg = RetCode.SUCCESS.message;
		this.retData = retData;
	}

	public BaseOutput(String retCode, String retMsg) {
		this.retCode = retCode;
		this.retMsg = retMsg;
	}

	public BaseOutput(String retCode, String retMsg, T retData) {
		this.retCode = retCode;
		this.retMsg = retMsg;
		this.retData = retData;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public T getRetData() {
		return retData;
	}

	public void setRetData(T retData) {
		this.retData = retData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BaseOutput [retCode=");
		builder.append(retCode);
		builder.append(", retMsg=");
		builder.append(retMsg);
		builder.append(", retData=");
		builder.append(retData);
		builder.append("]");
		return builder.toString();
	}

}
