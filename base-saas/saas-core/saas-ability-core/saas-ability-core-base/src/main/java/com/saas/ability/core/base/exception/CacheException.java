package com.saas.ability.core.base.exception;

/**
 * 缓存异常
 *
 */
public class CacheException extends BaseException {

	private static final long serialVersionUID = -1067511722933959092L;

	public CacheException(String retCode,String message){
		super(retCode,message);
	}
	
	public CacheException(String message,Throwable cause){
		super(message,cause);
	}
}
