package com.saas.ability.core.base.util;

import java.math.BigDecimal;

public class RMBUtil {

	public static long double2Long(double amount){
		BigDecimal decimal = BigDecimal.valueOf(amount);
		return decimal.movePointRight(2).longValue();
	}
	
	public static double long2Double(long amount){
		BigDecimal decimal = BigDecimal.valueOf(amount);
		return decimal.movePointLeft(2).doubleValue();
	}
	/**
	 * 转化为分
	 * @param amount
	 * @return
	 */
	public static long toFen(double amount){
		BigDecimal decimal = BigDecimal.valueOf(amount);
		return decimal.movePointRight(2).longValue();
	}
	
	/**
	 * 转化为元
	 * @param amount
	 * @return
	 */
	public static double toYuan(long amount){
		BigDecimal decimal = BigDecimal.valueOf(amount);
		return decimal.movePointLeft(2).doubleValue();
	}

	/**
	 * 元转换成分
	 *
	 * @param amount
	 * @return
	 */
	public static String toCents(String amount) {
		if (amount == null) {
			return "";
		}
		// 金额转化为分为单位
		String currency = amount.replaceAll("\\$|\\￥|\\,", "");  //处理包含, ￥ 或者$的金额
		int index = currency.indexOf(".");
		int length = currency.length();
		Long amLong = 0l;
		if (index == -1) {
			amLong = Long.valueOf(currency + "00");
		} else if (length - index >= 3) {
			amLong = Long.valueOf((currency.substring(0, index + 3)).replace(".", ""));
		} else if (length - index == 2) {
			amLong = Long.valueOf((currency.substring(0, index + 2)).replace(".", "") + 0);
		} else {
			amLong = Long.valueOf((currency.substring(0, index + 1)).replace(".", "") + "00");
		}
		return amLong.toString();
	}

	/**
	 *
	 * 功能描述：金额字符串转换：单位分转成单元

	 * @param o 传入需要转换的金额字符串
	 * @return 转换后的金额字符串
	 */
	public static String fenToYuan(Object o) {
		if(o == null)
			return "0.00";
		String s = o.toString();
		int len = -1;
		StringBuilder sb = new StringBuilder();
		if (s != null && s.trim().length()>0 && !s.equalsIgnoreCase("null")){
			s = removeZero(s);
			if (s != null && s.trim().length()>0 && !s.equalsIgnoreCase("null")){
				len = s.length();
				int tmp = s.indexOf("-");
				if(tmp>=0){
					if(len==2){
						sb.append("-0.0").append(s.substring(1));
					}else if(len==3){
						sb.append("-0.").append(s.substring(1));
					}else{
						sb.append(s.substring(0, len-2)).append(".").append(s.substring(len-2));
					}
				}else{
					if(len==1){
						sb.append("0.0").append(s);
					}else if(len==2){
						sb.append("0.").append(s);
					}else{
						sb.append(s.substring(0, len-2)).append(".").append(s.substring(len-2));
					}
				}
			}else{
				sb.append("0.00");
			}
		}else{
			sb.append("0.00");
		}
		return sb.toString();
	}

	/**
	 *
	 * 功能描述：去除字符串首部为"0"字符

	 * @param str 传入需要转换的字符串
	 * @return 转换后的字符串
	 */
	public static String removeZero(String str){
		char  ch;
		String result = "";
		if(str != null && str.trim().length()>0 && !str.trim().equalsIgnoreCase("null")){
			try{
				for(int i=0;i<str.length();i++){
					ch = str.charAt(i);
					if(ch != '0'){
						result = str.substring(i);
						break;
					}
				}
			}catch(Exception e){
				result = "";
			}
		}else{
			result = "";
		}
		return result;

	}
}
