package com.saas.ability.core.base.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.oss.OSSClient;


@RefreshScope
@Configuration
@ConditionalOnProperty(prefix="asssembly.aliyunoss",value="enable",matchIfMissing=true)
@EnableConfigurationProperties(value = AliyunossProperties.class)
public class AliyunossAutoConfigure {

	@RefreshScope
	@Bean
	public OSSClient creatOSSclient(AliyunossProperties aliyunossProperties){
		return new OSSClient(aliyunossProperties.getEndpoint(),
				aliyunossProperties.getAccessKeyId(),
				aliyunossProperties.getAccessKeySecret());
	}
}
