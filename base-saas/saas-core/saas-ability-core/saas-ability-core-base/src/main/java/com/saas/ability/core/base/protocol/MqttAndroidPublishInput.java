package com.saas.ability.core.base.protocol;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * mqtt安卓设备
 *
 */
@Setter
@Getter
@ToString
public class MqttAndroidPublishInput extends MqttPublishInput {

    /**
     * 任务类型
     *
     */
    private Integer msgType;

    /**
     * 设备sn
     */
    private String deviceSn;
}