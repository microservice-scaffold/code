package com.saas.ability.core.base.util;

import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 */
public class JsonUtils {
	
	public static final String enc = "utf-8";
	
	public static JSONObject jsonToMap(String jsonParams) {
		try {
			if (!StringUtils.isEmpty(jsonParams)) {
				return JSON.parseObject(EncodeUtil.decodeBase64String(jsonParams));
			} else {
				return new JSONObject();
			}
		} catch (Exception e) {
			throw new RuntimeException("参数解析异常!",e);
		}
	}

	public static JSONArray decodeToJSONArray(String content) {
		String result = EncodeUtil.decodeUrl(content);
		return JSONArray.parseArray(result);
	}

	public static JSONObject decodeToJSONObject(String content) {
		String result = EncodeUtil.decodeUrl(content);
		return JSONObject.parseObject(result);
	}

}
