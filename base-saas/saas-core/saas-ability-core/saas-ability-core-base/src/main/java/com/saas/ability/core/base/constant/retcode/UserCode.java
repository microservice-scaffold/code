package com.saas.ability.core.base.constant.retcode;

/**
 * 用户相关返回码
 *
 */
public enum UserCode {
	
	LOGOUT_FAIL("USER-001","用户登录失败"),
	
	AUTH_FAIL("AUTH-1000","用户鉴权失败"),
	
	USER_NOT_FOUND("AUTH-1001","用户不存在"),
	
	USER_ACOUNT_INVALID("AUTH-1002","用户不存在或密码错误"),
	
    USER_ACCOUNT_STOP_USE("USER-1003","账号停用"),
    
    USER_ACCOUNT_DISABLED("USER-1004","账号禁用"),
    
    USER_TOKEN_INVALID("AUTH-1005","登录会话失效")
	;

	public String retCode;
	
	public String message;
	
	private UserCode(String retCode,String message){
		this.retCode = retCode;
		this.message = message;
	}
}
