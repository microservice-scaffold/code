package com.saas.ability.core.base.protocol;
/**
 */
public class RsyncBaseOutput {

	protected String retCode;
	
	protected String retMsg;
	
	protected String retData;

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getRetData() {
		return retData;
	}

	public void setRetData(String retData) {
		this.retData = retData;
	}
}
