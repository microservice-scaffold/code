package com.saas.ability.core.base.view;

import java.util.Map;

import groovy.text.Template;
import groovy.text.TemplateEngine;

public class SimpleGroovyView implements View {

	private Template template;

	private Long lastTime;

	public SimpleGroovyView(TemplateEngine templateEngine, String tplContent) {
		try {
			if (this.template == null) {
				this.template = templateEngine.createTemplate(tplContent);
			}
		} catch (Exception e) {
			throw new RuntimeException("创建模版对象异常", e);
		}
	}

	@Override
	public String render(Map<String, Object> dataModel) {
		if (dataModel != null) {
			return template.make(dataModel).toString();
		} else {
			return template.make().toString();
		}
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

}
