package com.saas.ability.core.base.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="saas.ability")
public class SaasAbilityProperties {
	
	/**
	 * 最大缓存的模版视图实例
	 */
	public static final int DEFAULT_CACHE_LIMIT = 1024;

	private Template template;

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public static class Template {

		private String resourceLoadPath;

		private int cacheLimit = DEFAULT_CACHE_LIMIT;

		public String getResourceLoadPath() {
			return resourceLoadPath;
		}

		public void setResourceLoadPath(String resourceLoadPath) {
			this.resourceLoadPath = resourceLoadPath;
		}

		public int getCacheLimit() {
			return cacheLimit;
		}

		public void setCacheLimit(int cacheLimit) {
			this.cacheLimit = cacheLimit;
		}

	}
	
	
}
