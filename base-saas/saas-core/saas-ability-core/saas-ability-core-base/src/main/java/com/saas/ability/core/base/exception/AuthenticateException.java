package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

/**
 * 安全异常
 *
 */
public class AuthenticateException extends BaseException {

	private static final long serialVersionUID = 1230681282865679535L;

	public AuthenticateException(){
		super(RetCode.AUTH_INVALID.retCode,RetCode.AUTH_INVALID.message);
	}
	
	public AuthenticateException(String message){
		super(RetCode.AUTH_INVALID.retCode,message);
	}
	
	public AuthenticateException(String retCode,String message){
		super(retCode,message);
	}
}
