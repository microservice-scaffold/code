package com.saas.ability.core.base.constant.retcode;

/**
 * 公共返回码
 *
 */
public enum RetCode {

    SUCCESS("100", "操作成功"),

    HANDLE_FAIL("101", "操作失败"),

    FAIL("500", "服务器开小差，请稍后再试"),

    PARAM_INVALID("501", "参数不合法"),

    ORIGIN_INVALID("502", "接口请求来源不合法"),

    TOO_FREQUENTLY("503", "接口请求太频繁"),

    VERSION_INVALID("504", "版本不合法"),

    PERSISTENCE_FAIL("506", "持久层异常"),

    RESOURCE_NOT_FOUND("507", "资源不存在"),

    ENCODING_FAIL("508", "编码处理异常"),

    SIGN_FIAL("509", "签名验签发生异常"),

    VERIFY_INVALID("510", "验证数据不合法"),

    AUTH_INVALID("512", "鉴权不合法"),

    SERVICE_NOT_FOUND("513", "服务不存在"),

    MQ_SEND_FAIL("514", "MQ消息发送不成功"),

    SMS_SEND_FAIL("515", "短信发送不成功"),

    DEL_CACHE_FAIL("516", "删除缓存不成功"),

    ID_GENERATOR_FAIL("517", "ID生成失败"),

    NO_GENERATOR_FAIL("518", "NO生成失败"),

    DISTRIBUTED_LOCK("519", "分布式锁占用中"),

    PART_FAIL("520", "部分失败"),

    SCHEDULE_FAIL("530", "任务调度失败"),

    TOKEN_FAIL("600", "Token失效"),

    UNBIND_CSM_OPERATOR("700", "未绑定运营商"),

    DISABLED_CSM_OPERATOR("701", "运营商已被禁用"),

    UNDO_OPERATOR("702", "取消无效的操作"),
    
    INVALID_COMMAND("800", "操作结束"),
    
    TASK_BUSY("810", "任务繁忙"),
	
    DISPATCH_REPEAT("900", "重复调度"),
    
    DISPATCH_DELAY("901", "延迟调度"),
    
	DISPATCH_FAIL("902", "调度异常");

    public String retCode;

    public String message;

    private RetCode(String retCode, String message) {
        this.retCode = retCode;
        this.message = message;
    }

    public String getRetCode() {
        return retCode;
    }

    public String getMessage() {
        return message;
    }
}
