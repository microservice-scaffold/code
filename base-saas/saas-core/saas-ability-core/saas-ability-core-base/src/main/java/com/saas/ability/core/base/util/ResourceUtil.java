package com.saas.ability.core.base.util;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResourceUtil {

	public static String readData(String resourceLocation){
		try{
			File file = ResourceUtils.getFile("classpath:" + resourceLocation);
	        if(file.isFile()){
	        	List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
	        	if(lines != null){
	        		return lines.stream().map(line -> line).collect(Collectors.joining());
	        	}
	        }
	        return null;
		}catch(Exception e){
			log.error("读取文件异常", e);
			return null;
		}
	}
}
