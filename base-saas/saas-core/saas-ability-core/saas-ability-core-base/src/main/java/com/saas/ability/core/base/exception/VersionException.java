package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

/**
 * 版本验证异常
 *
 */
public class VersionException extends BaseException  {

	private static final long serialVersionUID = 370378930928155031L;


	public VersionException(Object obj){
		super(RetCode.VERSION_INVALID.retCode,RetCode.VERSION_INVALID.message);
	}

	public VersionException(String message,Object obj){
		super(RetCode.VERSION_INVALID.retCode,message);
	}

	public VersionException(String retCode, String message,Object obj){
		super(retCode,message);
	}
}
