package com.saas.ability.core.base.exception;

public class ServiceDataException extends RuntimeException {

    protected String retCode;

    protected String message;

    protected String retData;

    public ServiceDataException(String retCode,String message,String retData){
        this.retCode = retCode;
        this.message = message;
        this.retData = retData;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetData() {
        return retData;
    }

    public void setRetData(String retData) {
        this.retData = retData;
    }
}
