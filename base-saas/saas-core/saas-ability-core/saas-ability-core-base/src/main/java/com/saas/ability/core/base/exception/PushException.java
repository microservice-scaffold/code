package com.saas.ability.core.base.exception;

public class PushException extends BaseException {

	private static final long serialVersionUID = -1067511722933959092L;

	public PushException(String retCode,String message){
		super(retCode,message);
	}
	
	public PushException(String message,Throwable cause){
		super(message,cause);
	}
}
