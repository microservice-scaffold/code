package com.saas.ability.core.base.protocol;

import java.io.Serializable;

/**
 * 接口输入
 *
 */
public class BaseInput<T> implements Serializable {

	private static final long serialVersionUID = -1159223597593080626L;

	/**
	 * 应用ID
	 */
	private String appId;
	
	/**
	 * 设备唯一标识
	 */
	private String deviceSn;
	
	/**
	 * 设备ID
	 */
	private Integer deviceId;
	
	/**
	 * 编码
	 */
	private String charset;
	
	/**
	 * 终端版本号
	 */
	private String terminalVersion;
	
	/**
	 * 终端类型
	 */
	private Integer terminalType;
	
	/**
	 * 设备版本号
	 */
	private String deviceVersion;
	
	/**
	 * 后台接口API的版本号
	 */
	private String version;

	/**
	 * 时间戳
	 */
	private Long timesTamp;
	
	/**
	 * 登录令牌
	 */
	private String accessToken;
	
	/**
	 * 由原有的origin改为fromType请求来源
	 */
	private Integer fromType;


	/**
	 * 系统类型
	 */
	private Integer systemType;


	/**
	 * 微信OpenId
	 */
	private String openId;

	/**
	 * 品牌id
	 */
	private Integer groupId;
	
	/**
	 * 业务参数
	 */
	private T params;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDeviceSn() {
		return deviceSn;
	}

	public void setDeviceSn(String deviceSn) {
		this.deviceSn = deviceSn;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getTerminalVersion() {
		return terminalVersion;
	}

	public void setTerminalVersion(String terminalVersion) {
		this.terminalVersion = terminalVersion;
	}

	public String getDeviceVersion() {
		return deviceVersion;
	}

	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Long getTimesTamp() {
		return timesTamp;
	}

	public void setTimesTamp(Long timesTamp) {
		this.timesTamp = timesTamp;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public T getParams() {
		return params;
	}

	public void setParams(T params) {
		this.params = params;
	}

	public Integer getFromType() {
		return fromType;
	}

	public void setFromType(Integer fromType) {
		this.fromType = fromType;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(Integer terminalType) {
		this.terminalType = terminalType;
	}

	public Integer getSystemType() {
		return systemType;
	}

	public void setSystemType(Integer systemType) {
		this.systemType = systemType;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("{");
		sb.append("\"appId\":\"")
				.append(appId).append('\"');
		sb.append(",\"deviceSn\":\"")
				.append(deviceSn).append('\"');
		sb.append(",\"deviceId\":")
				.append(deviceId);
		sb.append(",\"charset\":\"")
				.append(charset).append('\"');
		sb.append(",\"terminalVersion\":\"")
				.append(terminalVersion).append('\"');
		sb.append(",\"terminalType\":")
				.append(terminalType);
		sb.append(",\"deviceVersion\":\"")
				.append(deviceVersion).append('\"');
		sb.append(",\"version\":\"")
				.append(version).append('\"');
		sb.append(",\"timesTamp\":")
				.append(timesTamp);
		sb.append(",\"accessToken\":\"")
				.append(accessToken).append('\"');
		sb.append(",\"fromType\":")
				.append(fromType);
		sb.append(",\"systemType\":")
				.append(systemType);
		sb.append(",\"openId\":\"")
				.append(openId).append('\"');
		sb.append(",\"groupId\":")
				.append(groupId);
		sb.append(",\"params\":")
				.append(params);
		sb.append('}');
		return sb.toString();
	}
}
