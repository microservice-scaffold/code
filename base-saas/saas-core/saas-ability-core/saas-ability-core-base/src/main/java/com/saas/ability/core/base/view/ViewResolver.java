package com.saas.ability.core.base.view;


/**
 * 模版视图转器
 *
 */
public interface ViewResolver {

	/**
	 * 根据模版名获取模版视图
	 * @param viewName
	 * @return
	 * @throws Exception
	 */
	View resolveViewName(String viewName,String tplContent);
	
	/**
	 * 获取最近一次的视图模版对象
	 * @param viewName
	 * @param tplContent
	 * @param lastTime
	 * @return
	 */
	View resolveViewName(String viewName,String tplContent,Long lastTime);
	
}
