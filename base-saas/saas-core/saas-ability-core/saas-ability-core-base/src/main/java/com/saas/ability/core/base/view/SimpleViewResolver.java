package com.saas.ability.core.base.view;

import groovy.text.SimpleTemplateEngine;
import groovy.text.TemplateEngine;

public class SimpleViewResolver extends AbstractCachingViewResolver {

	protected TemplateEngine templateEngine = new SimpleTemplateEngine();

	@Override
	public View createView(String tplContent,Long lastTime) {
		logger.debug("========= 创建视图模版 ===============");
		SimpleGroovyView view = new SimpleGroovyView(templateEngine,tplContent);
		if(lastTime == null){
			view.setLastTime(System.currentTimeMillis());
		}else{
			view.setLastTime(lastTime);
		}
		return view;
	}

}
