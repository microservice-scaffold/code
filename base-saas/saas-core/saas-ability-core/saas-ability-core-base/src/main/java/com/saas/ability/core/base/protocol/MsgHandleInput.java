package com.saas.ability.core.base.protocol;

import java.io.Serializable;

/**
 * 接口输入
 *
 */
public class MsgHandleInput<T> implements Serializable {

	private static final long serialVersionUID = -1159223597593080626L;

	private boolean isRetry;
	
	private String sence;
	
	private String actionType;
	
	private T data;

	public boolean isRetry() {
		return isRetry;
	}

	public void setRetry(boolean isRetry) {
		this.isRetry = isRetry;
	}

	public String getSence() {
		return sence;
	}

	public void setSence(String sence) {
		this.sence = sence;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MsgHandleInput [isRetry=");
		builder.append(isRetry);
		builder.append(", sence=");
		builder.append(sence);
		builder.append(", actionType=");
		builder.append(actionType);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}
	
	

}
