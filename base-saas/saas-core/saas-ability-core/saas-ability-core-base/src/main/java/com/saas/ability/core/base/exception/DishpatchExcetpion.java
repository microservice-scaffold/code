package com.saas.ability.core.base.exception;

public class DishpatchExcetpion extends RuntimeException {

	private static final long serialVersionUID = 7032674576218704882L;

	protected String retCode;

	protected String message;

	public DishpatchExcetpion(String retCode, String message) {
		super(message);
		this.retCode = retCode;
		this.message = message;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
