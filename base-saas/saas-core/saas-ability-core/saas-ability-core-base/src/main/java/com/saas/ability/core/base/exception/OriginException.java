package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

/**
 * 来源检查不合法
 *
 */
public class OriginException extends BaseException {

	private static final long serialVersionUID = 5358023300203907695L;


	public OriginException(){
		super(RetCode.ORIGIN_INVALID.retCode,RetCode.ORIGIN_INVALID.message);
	}
	
	public OriginException(String message){
		super(RetCode.ORIGIN_INVALID.retCode,message);
	}
}
