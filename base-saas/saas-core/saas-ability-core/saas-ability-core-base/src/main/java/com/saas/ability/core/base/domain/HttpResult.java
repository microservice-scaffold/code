
package com.saas.ability.core.base.domain;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpStatus;
/**
*  
*/
public class HttpResult {
	/**
     * 状态码
     */
    private Integer status;
    /**
     * 返回数据
     */
    private String data;

    public HttpResult() {
    }

    public HttpResult(Integer status, String data) {
        this.status = status;
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpResult{" +
                "status=" + status +
                ", data='" + data + '\'' +
                '}';
    }

    /**
     * 处理请求结果
     * @param httpResult
     * @return
     */
    public static boolean handleCallThirdResultInfo(HttpResult httpResult){
        boolean flag = false;
        if(null != httpResult && httpResult.getStatus() == HttpStatus.SC_OK){
            String data = httpResult.getData();
            if(null != data && !"".equals(data)){
                JSONObject jsonObject = JSON.parseObject(data);
                if(null != jsonObject && "100".equals(jsonObject.getString("retCode"))){
                    flag = true;
                }
            }
        }
        return flag;
    }
}
