package com.saas.ability.core.base.util;

import com.saas.ability.core.base.protocol.HeaderBaseInput;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpInputMessage;

/**
 * @description: Header参数封装类
 */
public class HeaderParamUtil {

    /**
     *  
     * @description: 获取头部输入参数
     */
    public static HeaderBaseInput getHeaderBaseInput(HttpInputMessage inputMessage){
        HeaderBaseInput haderBaseInput = new HeaderBaseInput();
        //fromType
        String fromType = inputMessage.getHeaders().getFirst("fromType");
        if (StringUtils.isNotBlank(fromType)) {
            haderBaseInput.setFromType(Integer.parseInt(fromType));
        }
        //deviceSn
        String deviceSn = inputMessage.getHeaders().getFirst("deviceSn");
        if (StringUtils.isNotBlank(deviceSn)) {
            haderBaseInput.setDeviceSn(deviceSn);
        }
        //deviceId
        String deviceId = inputMessage.getHeaders().getFirst("deviceId");
        if (StringUtils.isNotBlank(deviceId)) {
            haderBaseInput.setDeviceId(Integer.parseInt(deviceId));
        }
        //appVersion
        String appVersion = inputMessage.getHeaders().getFirst("appVersion");
        if (StringUtils.isNotBlank(appVersion)) {
            haderBaseInput.setAppVersion(appVersion);
        }
        //deviceVersion
        String deviceVersion = inputMessage.getHeaders().getFirst("deviceVersion");
        if (StringUtils.isNotBlank(deviceVersion)) {
            haderBaseInput.setDeviceVersion(deviceVersion);
        }
        //apiVersion
        String apiVersion = inputMessage.getHeaders().getFirst("apiVersion");
        if (StringUtils.isNotBlank(apiVersion)) {
            haderBaseInput.setApiVersion(apiVersion);
        }
        //deviceModel
        String deviceModel = inputMessage.getHeaders().getFirst("deviceModel");
        if (StringUtils.isNotBlank(deviceModel)) {
            haderBaseInput.setDeviceModel(deviceModel);
        }
        // deviceType
        String deviceType = inputMessage.getHeaders().getFirst("deviceType");
        if (StringUtils.isNotBlank(deviceType)) {
            haderBaseInput.setDeviceType(Integer.parseInt(deviceType));
        }
        // terminalType
        String terminalType = inputMessage.getHeaders().getFirst("terminalType");
        if (StringUtils.isNotBlank(terminalType)) {
            haderBaseInput.setTerminalType(Integer.parseInt(terminalType));
        }

        // systemType
        String systemType = inputMessage.getHeaders().getFirst("systemType");
        if (StringUtils.isNotBlank(systemType)) {
            haderBaseInput.setSystemType(Integer.parseInt(systemType));
        }

        return haderBaseInput;
    }
}
