package com.saas.ability.core.base.util;

public class VersionUtils {

    /**
     * 版本号比较
     *
     * @param appVersion    app传递的版本号
     * @param serverVersion 服务器端版本号
     * @return 0代表相等，1代表左边大，-1代表右边大
     * Utils.compareVersion("1.0.358_20180820090554","1.0.358_20180820090553")=1
     */
    public static int compareVersion(String appVersion, String serverVersion) {
        if (appVersion.equals(serverVersion)) {
            return 0;
        }

        String v1 = appVersion.replace("V", "").replace("v", "");
        String v2 = serverVersion.replace("V", "").replace("v", "");

        String[] version1Array = v1.split("[.]");
        String[] version2Array = v2.split("[.]");
        int index = 0;
        int minLen = Math.min(version1Array.length, version2Array.length);
        long diff = 0;

        while (index < minLen
                && (diff = Long.parseLong(version1Array[index])
                - Long.parseLong(version2Array[index])) == 0) {
            index++;
        }
        if (diff == 0) {
            for (int i = index; i < version1Array.length; i++) {
                if (Long.parseLong(version1Array[i]) > 0) {
                    return 1;
                }
            }

            for (int i = index; i < version2Array.length; i++) {
                if (Long.parseLong(version2Array[i]) > 0) {
                    return -1;
                }
            }
            return 0;
        } else {
            return diff > 0 ? 1 : -1;
        }
    }

    public static void main(String[] args) {
        System.out.println(compareVersion("V1.0.359.554","v1.0.359.554"));
    }

}
