package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class ParameterException extends ServiceException {
	
	private static final long serialVersionUID = 3488330172370689641L;
	
	private static final String PREFIX = "PARAM-";

	public ParameterException(){
		super(PREFIX + RetCode.PARAM_INVALID.retCode,RetCode.PARAM_INVALID.message);
	}
	
	public ParameterException(String message){
		super(PREFIX + RetCode.PARAM_INVALID.retCode,message);
	}
	
	public ParameterException(String retCode,String message){
		super(retCode,message);
	}
}
