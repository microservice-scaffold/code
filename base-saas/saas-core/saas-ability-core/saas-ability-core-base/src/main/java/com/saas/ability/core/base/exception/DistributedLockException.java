package com.saas.ability.core.base.exception;

/**
 * 分布式锁异常
 *
 */
public class DistributedLockException extends RuntimeException {

    protected String retCode;

    protected String message;

    public DistributedLockException(String retCode, String message) {
        this.retCode = retCode;
        this.message = message;
    }

    public String getRetCode() {
        return retCode;
    }

    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}