package com.saas.ability.core.base.http;

public class Resp {

	public final static int SUCC = 100;

	public final static int FAIL = -500;

	int statusCode;

	int retCode;

	String retData;

	public Resp() {
		this.statusCode = 200;
		this.retCode = Resp.SUCC;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}

	public String getRetData() {
		return retData;
	}

	public void setRetData(String retData) {
		this.retData = retData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Resp [statusCode=");
		builder.append(statusCode);
		builder.append(", retCode=");
		builder.append(retCode);
		builder.append(", retData=");
		builder.append(retData);
		builder.append("]");
		return builder.toString();
	}

}
