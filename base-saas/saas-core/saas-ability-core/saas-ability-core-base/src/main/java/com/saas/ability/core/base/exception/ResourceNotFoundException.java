package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class ResourceNotFoundException extends BaseException {

	private static final long serialVersionUID = -8725404983358518655L;
	
	private static final String PREFIX = "RESOURCE-";

	public ResourceNotFoundException(){
		super(PREFIX + RetCode.RESOURCE_NOT_FOUND.retCode,RetCode.RESOURCE_NOT_FOUND.message);
	}
	
	public ResourceNotFoundException(String message){
		super(PREFIX + RetCode.RESOURCE_NOT_FOUND.retCode,message);
	}
	
	public ResourceNotFoundException(String retCode,String message){
		super(retCode,message);
	}
}
