package com.saas.ability.core.base.util;

import java.util.List;
import java.util.Set;

public class ListUtil {
	
	private ListUtil(){}
	
	public static String listToString(List<?> list, char separator) {
		if(list == null || list.isEmpty()){
			return "";
		}
		
        StringBuilder sb = new StringBuilder();
        list.forEach(v-> {
        	sb.append(v).append(separator);
        });
        
        return sb.toString().substring(0, sb.toString().length() - 1);
    }
	
	public static String setToString(Set<?> list, char separator) {
		if(list == null || list.isEmpty()){
			return "";
		}
		
        StringBuilder sb = new StringBuilder();
        list.forEach(v-> {
        	sb.append(v).append(separator);
        });
        
        return sb.toString().substring(0, sb.toString().length() - 1);
    }
}
