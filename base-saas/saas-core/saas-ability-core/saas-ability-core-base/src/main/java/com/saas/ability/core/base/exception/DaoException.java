package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class DaoException extends ServiceException {
	
	private static final long serialVersionUID = -6609597141327135819L;
	
	
	private static final String PREFIX = "DAO-";

	public DaoException(){
		super(PREFIX + RetCode.PERSISTENCE_FAIL.retCode,RetCode.PERSISTENCE_FAIL.message);
	}
	
	public DaoException(String message){
		super(PREFIX + RetCode.PERSISTENCE_FAIL.retCode,message);
	}
	
	public DaoException(String retCode,String message){
		super(PREFIX +retCode,message);
	}
}
