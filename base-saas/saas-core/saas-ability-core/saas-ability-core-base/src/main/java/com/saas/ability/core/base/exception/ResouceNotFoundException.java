package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class ResouceNotFoundException extends ServiceException {

	private static final long serialVersionUID = -8725404983358518655L;
	
	private static final String PREFIX = "RES-";

	public ResouceNotFoundException(String retCode,String message){
		super(retCode,message);
	}
	
	public ResouceNotFoundException(){
		super(PREFIX + RetCode.RESOURCE_NOT_FOUND.retCode,RetCode.RESOURCE_NOT_FOUND.message);
	}
}
