package com.saas.ability.core.base.util;

import java.util.UUID;

/**
 * 令牌生成
 *
 */
public class TokenUtil {

	/**
	 * 生成访问令牌
	 * @return
	 */
	public static String getAccessToken(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
