package com.saas.ability.core.base.protocol;

/**
 * 描述:抽象带分页参数对象
 *
 */
public abstract class PageInput {
    /**
     * 当前页
     */
    private Integer pageNo = 1;
    /**
     * 每页显示行数
     */
    private Integer pageSize = 20;
    
    
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
    
    
}
