package com.saas.ability.core.base.protocol;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * mqtt
 */
@Setter
@Getter
@ToString
public class MqttCardPublishInput extends MqttPublishInput {

    /**
     * 任务类型

     */
    private Integer taskType;

    /**
     * 设备sn
     */
    private String deviceSn;
}