package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class GeneralException extends ServiceException {

	private static final long serialVersionUID = -589179841447788470L;
	
	private static final String PREFIX = "GENERAL-";

	public GeneralException(){
		super(PREFIX + RetCode.FAIL.retCode,RetCode.FAIL.message);
	}
	
	public GeneralException(String message){
		super(PREFIX + RetCode.FAIL.retCode,message);
	}
}
