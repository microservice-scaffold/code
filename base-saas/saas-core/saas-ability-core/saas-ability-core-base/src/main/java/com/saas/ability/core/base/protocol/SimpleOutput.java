package com.saas.ability.core.base.protocol;

/**
 * 接口输出通用实现
 *
 */
public class SimpleOutput extends BaseOutput<String> {

	private static final long serialVersionUID = 4404955550745587553L;

	public SimpleOutput(){
	}
	
	public SimpleOutput(String retCode, String retMsg) {
		super(retCode,retMsg);
	}

	public SimpleOutput(String retCode, String retMsg, String retData) {
		super(retCode,retMsg,retData);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SimpleOutput [retCode=");
		builder.append(retCode);
		builder.append(", retMsg=");
		builder.append(retMsg);
		builder.append(", retData=");
		builder.append(retData);
		builder.append("]");
		return builder.toString();
	}
	
	
}
