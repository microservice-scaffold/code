package com.saas.ability.core.base.protocol;


import java.io.Serializable;
import java.util.HashMap;

/**
 * 为了满足2.0的接口返回
 *
 */
public class RespOutput implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -5179561291924217001L;
	



    /**
     * 结果状态
     */
    private String status;
    /**
     * 提示信息
     */
    private String info;

    /**
     * 扩展信息
     */
    private String extInfo = "";



    /**
     * 数据对象
     */
    private Object response;




    /**
     * 默认构造方法
     */
    public RespOutput(){}

    /**
     * 带参数的构造方法
     * @param status 装态码
     * @param info   返回提示信息
     */
    public RespOutput(String status, String info){
        this.status = status;
        this.info = info;
    }
    public RespOutput(String status, String info, Object response){
        this.status = status;
        this.info = info;
        this.response = response;
    }

    public Object getResponse() {
        if(null == this.response){
            return new HashMap<>();
        }
        return response;
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getExtInfo() {
		return extInfo;
	}

	public void setExtInfo(String extInfo) {
		this.extInfo = extInfo;
	}

	public void setResponse(Object response) {
		this.response = response;
	}



}
