package com.saas.ability.core.base.util;

public class ObjectUtil {

	public static String toStr(Object val){
		if(val != null){
			return val.toString();
		}
		return null;
	}
	
	public static String toStr(Object val,String deftVal){
		if(val != null){
			return val.toString();
		}
		return deftVal;
	}

}
