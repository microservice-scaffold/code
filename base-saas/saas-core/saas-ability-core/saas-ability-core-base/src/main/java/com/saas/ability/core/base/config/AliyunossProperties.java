package com.saas.ability.core.base.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="asssembly.aliyunoss")
public class AliyunossProperties {

	private String accessKeyId;

	private String accessKeySecret;

	private String endpoint;

	private String preCommentKey;

	private String mappoint1;

	private String bucketName;

	private String prePurOrderKey;

	private String preOrderEvaluateKey;

	private String preSupplierKey;

	private String preUserKey;

	private String preVisitKey;

	private String preResAbnormalCloseKey;

	private String preResAbnormalCloseSumKey;

	private String preCommonExcelKey;

	private String preDeviceKey;

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public String getAccessKeySecret() {
		return accessKeySecret;
	}

	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getPreCommentKey() {
		return preCommentKey;
	}

	public void setPreCommentKey(String preCommentKey) {
		this.preCommentKey = preCommentKey;
	}

	public String getMappoint1() {
		return mappoint1;
	}

	public void setMappoint1(String mappoint1) {
		this.mappoint1 = mappoint1;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getPrePurOrderKey() {
		return prePurOrderKey;
	}

	public void setPrePurOrderKey(String prePurOrderKey) {
		this.prePurOrderKey = prePurOrderKey;
	}

	public String getPreOrderEvaluateKey() {
		return preOrderEvaluateKey;
	}

	public void setPreOrderEvaluateKey(String preOrderEvaluateKey) {
		this.preOrderEvaluateKey = preOrderEvaluateKey;
	}

	public String getPreSupplierKey() {
		return preSupplierKey;
	}

	public void setPreSupplierKey(String preSupplierKey) {
		this.preSupplierKey = preSupplierKey;
	}

	public String getPreUserKey() {
		return preUserKey;
	}

	public void setPreUserKey(String preUserKey) {
		this.preUserKey = preUserKey;
	}

	public String getPreVisitKey() { return preVisitKey; }

	public void setPreVisitKey(String preVisitKey) {
		this.preVisitKey = preVisitKey;
	}

	public String getPreCommonExcelKey() {
		return preCommonExcelKey;
	}

	public AliyunossProperties setPreCommonExcelKey(String preCommonExcelKey) {
		this.preCommonExcelKey = preCommonExcelKey;
		return this;
	}

	public String getPreDeviceKey() { return preDeviceKey; }

	public void setPreDeviceKey(String preDeviceKey) { this.preDeviceKey = preDeviceKey; }

	public String getPreResAbnormalCloseKey() {
		return preResAbnormalCloseKey;
	}

	public void setPreResAbnormalCloseKey(String preResAbnormalCloseKey) {
		this. preResAbnormalCloseKey = preResAbnormalCloseKey;
	}

	public String getPreResAbnormalCloseSumKey() {
		return preResAbnormalCloseSumKey;
	}

	public void setPreResAbnormalCloseSumKey(String preResAbnormalCloseSumKey) {
		this.preResAbnormalCloseSumKey = preResAbnormalCloseSumKey;
	}
}
