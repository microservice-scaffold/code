package com.saas.ability.core.base.util;

import java.util.UUID;

/**
 * UUID
 *
 */
public class UUIDGenerator {

    /**
     * 生成去除“-”的UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}