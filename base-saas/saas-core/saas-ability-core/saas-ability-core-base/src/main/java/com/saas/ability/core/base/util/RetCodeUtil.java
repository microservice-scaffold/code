package com.saas.ability.core.base.util;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class RetCodeUtil {

	public static boolean isSucc(String retCode){
		return retCode.equals(RetCode.SUCCESS.retCode);
	}
}
