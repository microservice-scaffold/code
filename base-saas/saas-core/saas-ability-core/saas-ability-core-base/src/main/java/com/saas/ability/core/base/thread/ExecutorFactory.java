

package com.saas.ability.core.base.thread;

import java.util.concurrent.Executor;

public interface ExecutorFactory {
	
    String EVENT_BUS = "event-bus";
    
    String EXECUTOR_TYPE = "scheduled";

    Executor get(String name,String executorType);
}
