package com.saas.ability.core.base.event;

import org.springframework.context.ApplicationEvent;

public class MessageEvent<T> extends ApplicationEvent {

	private static final long serialVersionUID = 1213557485252354744L;

	public MessageEvent(T source) {
		super(source);
	}
}
