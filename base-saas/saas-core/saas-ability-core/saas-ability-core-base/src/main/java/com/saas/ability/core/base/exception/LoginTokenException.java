package com.saas.ability.core.base.exception;


import com.saas.ability.core.base.constant.retcode.UserCode;

public class LoginTokenException extends ServiceException {

    private static final long serialVersionUID = -8725404983358518655L;

    public LoginTokenException() {
        super(UserCode.USER_TOKEN_INVALID.retCode, UserCode.USER_TOKEN_INVALID.message);
    }

    public LoginTokenException(String message) {
        super(UserCode.USER_TOKEN_INVALID.retCode, message);
    }

    public LoginTokenException(String retCode, String message) {
        super(retCode, message);
    }
}
