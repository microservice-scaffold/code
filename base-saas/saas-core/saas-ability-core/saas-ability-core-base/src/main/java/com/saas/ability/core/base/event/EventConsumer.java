

package com.saas.ability.core.base.event;

public abstract class EventConsumer {

    public EventConsumer() {
        EventBus.register(this);
    }

}
