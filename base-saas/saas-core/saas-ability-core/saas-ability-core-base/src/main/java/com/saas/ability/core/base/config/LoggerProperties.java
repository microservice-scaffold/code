package com.saas.ability.core.base.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "saas.log")
public class LoggerProperties {

	private String logConfig;

	private String logHome;

	public String getLogConfig() {
		return logConfig;
	}

	public void setLogConfig(String logConfig) {
		this.logConfig = logConfig;
	}

	public String getLogHome() {
		return logHome;
	}

	public void setLogHome(String logHome) {
		this.logHome = logHome;
	}

}
