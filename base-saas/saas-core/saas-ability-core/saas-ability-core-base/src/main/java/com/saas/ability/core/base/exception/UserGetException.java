package com.saas.ability.core.base.exception;

import com.saas.ability.core.base.constant.retcode.RetCode;

public class UserGetException extends ServiceException {
	
	private static final long serialVersionUID = 6428578923804415394L;
	
	private static final String PREFIX = "USER-";

	public UserGetException(){
		super(PREFIX + RetCode.RESOURCE_NOT_FOUND.retCode,RetCode.RESOURCE_NOT_FOUND.message);
	}
	
	public UserGetException(String message){
		super(PREFIX + RetCode.RESOURCE_NOT_FOUND.retCode,message);
	}
	
	public UserGetException(String retCode,String message){
		super(PREFIX +retCode,message);
	}
}
