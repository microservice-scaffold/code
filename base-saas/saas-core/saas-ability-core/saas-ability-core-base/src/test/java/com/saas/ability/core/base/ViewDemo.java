package com.saas.ability.core.base;

import com.saas.ability.core.base.view.SimpleViewResolver;
import com.saas.ability.core.base.view.ViewResolver;

public class ViewDemo {

	public static void main(String[] args) {
		ViewResolver resolver = new SimpleViewResolver();
		resolver.resolveViewName("demo-view", "hello-world");

		resolver.resolveViewName("demo-view", "hello-world");
		
		resolver.resolveViewName("demo-view", "hello-world",123456L);
		
		resolver.resolveViewName("demo-view", "hello-world",123456L);
	}
}
