### 项目简介
 
本项目为springcloud微服务生产级实际落地的脚手架，无私包，全开源，通用命名，非后台管理系统脚手架，是大规模用户、业务复杂的SAAS平台级脚手架。本项目提供了与业务无关的微服务基础必要模块组件；具体业务支持分库分包开发，支持中台服务复用，层次清晰，可复用性强；提供了细致的搭建运行文档、业务服务编码文档。

本项目可自由使用，无授权限制，但基于此项目的衍生开源项目需获得本项目发起人许可。

本项目的依赖jar包版本升级、微服务组件拓展升级，可根据实际需要，自行开发，欢迎分支开源。

本项目目前无业务相关代码，后期会在此脚手架基础上，逐步迭代电商与零售相关的业务，欢迎有兴趣的小伙伴一起参与。

如果项目有帮助到你，麻烦点个星。

### 项目技术

数据库mysql

缓存redis

消息中间件rabbitmq

框架中间件springboot+springmvc+spring+mybatis

微服务注册中心springcloud eureka

微服务配置中心springcloud config

微服务网关中心springcloud zuul

微服务监控中心springboot admin

微服务调用springcloud feign

微服务熔断springcloud hystrix

微服务链路追踪sleuth+zipkin

分布式事务seata

### 项目层级

![输入图片说明](%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(25).png)

### 业务层级

业务数据库--》业务实体包--》业务dao包--》业务缓存包--》业务service包--》原子中台服务（不同原子中台服务间可互相依赖调用）--》终端服务（可组装多个原子中台服务，不和数据库打交道）--》网关中心（对外服务接口）

### 快速启动

详细截图加微信dabinhenry获取

一、安装配置Jdk1.8

二、安装配置Maven

三、安装配置Idea

四、安装配置Mysql5.7

五、安装配置Redis

六、安装配置Rabbitmq

七、安装配置Svn

八、安装配置Git

九、安装配置Seata

十、本地域名映射

十一、下载代码，执行sql，搭建配置中心

十二、idea打包运行代码

十三、测试接口（正确保存到3个库；异常全局回滚；两种情况自行测试）


### 开发手册

 **项目配置** 

|base-saas

    |saas-config

        |..

            (根据实际情况修改，并同步到svn配置中心，包含redis、mysql、rabbitmq、zuul网关路由、seata等)

    |saas-ability

        |saas-ability-configure

            |application.yml

                (根据实际情况修改，svn配置中心的地址、用户名、密码、分支；mq配置等)

 **开发一个新服务** 

步骤1：建库建表

参考sql\5.sql和sql\6.sql，对应原子中台服务的数据库与终端服务的数据库，终端服务无具体表实现

步骤2：添加服务的配置文件至svn配置中心，并添加服务的网关映射配置

参考saas-config目录下的*gateway*.yml、*servicedemo*.yml、*platformservicedemo*.yml

步骤3：添加实际业务对应的实体、dao、缓存等项目

    3.1实际业务实体项目定义

    参考saas-data-domain-servicedemo

    3.2saas-data-domain的pom.xml
    
    参考pom文件，添加saas-data-domain-servicedemo子模块管理

    3.3实际业务dao项目定义

    参考saas-data-dao-servicedemo

    3.4saas-data-dao的pom.xml

    参考pom文件，添加saas-data-dao-servicedemo子模块管理

    3.5实际业务缓存项目定义

    参考saas-data-cache-servicedemo

    3.6saas-data-cache的pom.xml

    参考pom文件，添加saas-data-cache-servicedemo子模块管理

    3.7saas-data的pom.xml

    参考pom文件，添加saas-data-domain-servicedemo、saas-data-dao-servicedemo、saas-data-cache-servicedemo依赖定义

步骤4：编写接口协议及熔断降级规则

    4.1实际业务接口项目定义

    参考saas-api-op-servicedemo

    4.2saas-api-op的pom.xml

    参考pom文件，添加saas-api-op-servicedemo子模块管理

    4.3saas-api的pom.xml

    参考pom文件，添加saas-api-op-servicedemo依赖定义

步骤5：编写原子中台服务及终端服务

    5.1saas-op的pom.xml

    参考pom文件，添加saas-api-op-servicedemo依赖定义

    5.2saas-kernel的pom.xml

    参考pom文件，添加saas-kernel-servicedemo子模块管理

    5.3saas-platform的pom.xml

    参考pom文件，添加saas-platform-platformservicedemo子模块管理

    5.4实际业务原子中台服务项目定义

    参考saas-kernel-servicedemo

    5.5实际业务终端服务项目定义

    参考saas-platform-platformservicedemo

步骤6：测试接口
![输入图片说明](%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE(24).png)